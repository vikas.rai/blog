﻿using AI.CommonLayer.Generics;
using AI.DataLayer;
using BlogApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace BlogApi.Controllers
{
    public class Login
    {
        public string UserName { get; set; }
        public string Pwd { get; set; }
    }
    public class FileController : ApiController
    {
        [HttpGet]
        public async Task<IHttpActionResult> Recipes(long PageOffset, int PageSize, string Search)
        {
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "get");
                Params.Add("PageSize", PageSize);
                Params.Add("PageOffset", PageOffset);
                Params.Add("Search", Search);
                var DB = DatabaseFactory.GetDatabase();
                var lstRecipe = await DB.GetObjectList<Recipe>("proc_Recipes", Params);
                return Ok(lstRecipe);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public async Task<IHttpActionResult> Recipes(long ID)
        {
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "getbyid");
                Params.Add("ID", ID);
                var DB = DatabaseFactory.GetDatabase();
                var ds = await DB.GetDataSet("proc_Recipes", Params);
                var Recipe = ds.Tables[0].ToObject<Recipe>();
                Recipe.Comments = ds.Tables[1].ToList<RecipeComments>();
                return Ok(Recipe);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<IHttpActionResult> Recipes([FromBody]Recipe recipe)
        {
            if (recipe == null)
            {
                return BadRequest("recipe is null");
            }
            Dictionary<string, object> Params = new Dictionary<string, object>();
            if (recipe.ID > 0)
            {
                Params.Add("type", "update");
            }
            else
            {
                Params.Add("type", "insert");
            }
            Params.Add("Name", recipe.Name);
            Params.Add("ImgURL", recipe.ImgURL);
            Params.Add("ID", recipe.ID);
            Params.Add("Contents", recipe.Contents);
            var DB = DatabaseFactory.GetDatabase();
            var i = await DB.ExecuteNonQuery("proc_Recipes", Params);
            return Ok(true);
        }
        [HttpGet]
        public async Task<IHttpActionResult> DeleteRecipes(long ID)
        {
            if (ID == 0)
            {
                return BadRequest("recipe is null");
            }
            Dictionary<string, object> Params = new Dictionary<string, object>();
            Params.Add("type", "delete");
            Params.Add("ID", ID);
            var DB = DatabaseFactory.GetDatabase();
            var i = await DB.ExecuteNonQuery("proc_Recipes", Params);
            return Ok(true);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetMedia(long PageOffset, int PageSize, string Search)
        {
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "get");
                Params.Add("PageSize", PageSize);
                Params.Add("PageOffset", PageOffset);
                Params.Add("Search", Search);
                var DB = DatabaseFactory.GetDatabase();
                var lstMedia = await DB.GetObjectList<MediaFile>("proc_MediaFile", Params);
                return Ok(lstMedia);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> Upload()
        {
            if (HttpContext.Current.Request.Files.Count == 0)
            {
                return BadRequest("file not received");
            }
            try
            {
                var Spath = HttpContext.Current.Server.MapPath("BlogImages/");
                if (!System.IO.Directory.Exists(Spath))
                {
                    System.IO.Directory.CreateDirectory(Spath);
                }
                var FilePath = DateTime.UtcNow.ToString("dd-MM-yyyy hhmmsstt") + " " + HttpContext.Current.Request.Files[0].FileName;
                Spath += FilePath;
                HttpContext.Current.Request.Files[0].SaveAs(Spath);

                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "insert");
                Params.Add("FileName", HttpContext.Current.Request.Files[0].FileName);
                Params.Add("FilePath", FilePath);
                var DB = DatabaseFactory.GetDatabase();
                var i = await DB.ExecuteNonQuery("proc_MediaFile", Params);
                return Ok(FilePath);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        public HttpResponseMessage Download(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent("path is empty") };
            }
            try
            {
                var Spath = HttpContext.Current.Server.MapPath("BlogImages/");
                Spath += path;
                var bytes = System.IO.File.ReadAllBytes(Spath);
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new ByteArrayContent(bytes);
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(MimeMapping.GetMimeMapping(Spath));

                return response;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest) { Content = new StringContent(ex.Message) };
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> SendEmail(string Name, string Email, string Mobile, string Message)
        {
            var Status = false;
            try
            {
                var smtpMail = new SmtpMail();
                smtpMail.SslEnabled = false;
                smtpMail.Subject = ConfigurationManager.AppSettings["MailSubject"];
                smtpMail.To = System.Configuration.ConfigurationManager.AppSettings["MailTo"];
                smtpMail.Body = $@"
                                    Hi Team<br>
                                    {Name} has been sent a message given below-<br>
                                    Mobile Number- {Mobile}<br>
                                    Email Address- {Email}<br>
                                    Message- {Message}
                            ";
                Status = await smtpMail.Send();
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
            return Ok(Status);
        }
        [HttpGet]
        public async Task<IHttpActionResult> Subscribe(string Name, string Email)
        {
            var i = 0L;
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "insert");
                Params.Add("Name", Name);
                Params.Add("Email", Email);
                var DB = DatabaseFactory.GetDatabase();
                i = Convert.ToInt64(await DB.ExecuteScalar("proc_Subscribe", Params));

            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
            return Ok(i);
        }
        [HttpGet]
        public async Task<IHttpActionResult> RecipeComments(long RecipeID, long UserID, long Rating, string Comment, string CreatedDate)
        {
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "insert");
                Params.Add("RecipeID", RecipeID);
                Params.Add("userID", UserID);
                Params.Add("comment", Comment);
                Params.Add("Rating", Rating);
                Params.Add("CreatedDate", CreatedDate);
                var DB = DatabaseFactory.GetDatabase();
                var i = await DB.ExecuteNonQuery("proc_recipeComments", Params);
                return Ok(i > 0);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }

        }
        [HttpGet]
        public async Task<IHttpActionResult> GetSubscribe()
        {
            var Count = 0L;
            try
            {
                Dictionary<string, object> Params = new Dictionary<string, object>();
                Params.Add("type", "get");
                var DB = DatabaseFactory.GetDatabase();
                var res = await DB.ExecuteScalar("proc_Subscribe", Params);
                Count = Convert.ToInt64(res);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
            }
            return Ok(Count);
        }
        [HttpGet]
        public async Task<IHttpActionResult> SignIn(string UserName, string Pwd)
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Pwd))
            {
                return BadRequest("Invalid UserName or password.");
            }

            try
            {
                var path = HttpContext.Current.Server.MapPath("login/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                path = Path.Combine(path, "Auth.json");
                if (!File.Exists(path))
                {
                    File.WriteAllText(path, "[ { 'UserName': 'rahul.rai', 'pwd': 'Rahul@123', 'email': 'rahulrai1304@gmail.com' }, { 'UserName': 'vikas.rai', 'pwd': '123', 'email': 'vikas.rai@outlook.com' } ]");
                }
                var authJson = JsonConvert.DeserializeObject<List<Login>>(File.ReadAllText(path));
                var user = authJson.Where(x => x.UserName == UserName && x.Pwd == Pwd).FirstOrDefault();
                if (user != null)
                {
                    return Ok(true);
                }
                else
                {
                    return BadRequest("Invalid UserName or password.");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(true);
        }


    }
}
