﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace BlogApi.Models
{
    public class SmtpMail
    {
        public SmtpMail()
        {
            MailServer = ConfigurationManager.AppSettings["MailServer"];
            From = ConfigurationManager.AppSettings["MailFrom"];
            MailPort = Convert.ToInt32(ConfigurationManager.AppSettings["MailPort"]);
            Password = ConfigurationManager.AppSettings["Password"];
        }
        public SmtpMail(string _Mailserver, string _From, int _mailPort, string _Password)
        {
            MailServer = _Mailserver;
            From = _From;
            MailPort = _mailPort;
            Password = _Password;
        }
        public string MailServer { get; set; }
        public int MailPort { get; set; }
        public bool? SslEnabled
        {
            set
            {
                _IsSsl = value;
            }
            get
            {
                return _IsSsl == null ? true : _IsSsl;
            }
        }
        public string From { get; set; }
        public string FromName { get; set; }
        public string To { get; set; }
        public string Cc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        private bool? _IsSsl = null;
        private bool? _IsBodyHtml = null;
        public bool? IsBodyHtml
        {
            set
            {
                _IsBodyHtml = value;
            }
            get
            {
                return _IsBodyHtml == null ? true : _IsBodyHtml;
            }
        }

        private MailAddress from
        {
            get
            {
                return new MailAddress(From, FromName);
            }
        }

        private void GetMailCollection(ref MailMessage Mail)
        {
            if (string.IsNullOrEmpty(To))
            {
                throw new Exception("To email can not be blank.");
            }
            var lst = To.Split(',');
            foreach (var item in lst)
            {
                Mail.To.Add(item);
            }
            if (!string.IsNullOrEmpty(Cc))
            {
                lst = Cc.Split(',');
                foreach (var item in lst)
                {
                    Mail.CC.Add(item);
                }
            }
        }

        public async Task<bool> Send()
        {
            var Status = false;
            await Task.Run(() =>
            {
                try
                {

                    MailMessage Mail = new MailMessage();
                    SmtpClient SmtpServer = new SmtpClient(MailServer);

                    Mail.From = from;
                    GetMailCollection(ref Mail);
                    Mail.Subject = Subject;
                    Mail.Body = Body;
                    Mail.IsBodyHtml = IsBodyHtml.Value;

                    SmtpServer.Port = MailPort;
                    SmtpServer.Credentials = new System.Net.NetworkCredential(string.IsNullOrEmpty(UserName) ? From : UserName, Password);
                    SmtpServer.EnableSsl = SslEnabled.Value;

                    SmtpServer.Send(Mail);
                    Status = true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            });

            return Status;
        }
    }

}