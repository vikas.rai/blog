﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlogApi.Models
{
    public class MediaFile
    {
        public long ID { get; set; }
        public long TotalCount { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedOn { get; set; }
    }
    public class Recipe
    {
        public Recipe()
        {
            Comments = new List<RecipeComments>();
        }
        public long ID { get; set; }
        public long TotalCount { get; set; }
        public string ImgURL { get; set; }
        public string Name { get; set; }
        public string Rating { get; set; }
        public string Contents { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<RecipeComments> Comments { get; set; }

    }
    public class RecipeComments
    {
        public long ID { get; set; }
        public long RecipeID { get; set; }
        public long UserID { get; set; }
        public string Name{ get; set; }
        public long Rating { get; set; }
        public string Comment { get; set; }
        public string CreatedDate { get; set; }
    }

}