import { Component, OnInit } from '@angular/core';
import { common } from '../common-model';
import { vModal } from '../vikas-modal';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  login() {
    var UserName = $('#UserName').val();
    var Pwd = $('#Pwd').val();
    if (UserName == '') {
      vModal.Warning("UserName Required.", undefined);
      return;
    }
    if (Pwd == '') {
      vModal.Warning("Password Required.", undefined);
      return;
    }
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/SignIn?UserName=" + UserName + "&Pwd=" + Pwd,
      data: data,
      success: function (data) {
        if (data) {
          common.SetCookie("isLoggedIN", data, (24 * 7));
          common.SetCookie("UserName", UserName, (24 * 7));
          location.href = location.origin + "/#/Recipes"
        }
        else {
          eval('app.vModal.Error("Invalid UserName or Password", undefined);');
        }

      },
      error: function (e) {
        eval('app.vModal.Error("Invalid UserName or Password", undefined);');
      }
    });
  }
}
