export class VikasModal {
    Templet: HTMLDivElement;
    xyz: boolean;
    async Alert(Message, Type, OnClose, data = undefined) {
        this.Templet = this.getTemplate(Templates.Alert);
        document.body.appendChild(this.Templet);
        this.addEvent('.closeModal', 'click', this.Close, OnClose, data)
        this.Templet.querySelector('.modal-body').innerHTML = Message;
        if (Type == 'Success') {
            this.Templet.querySelector('.modal-title').innerHTML = 'Success';
            (this.Templet.querySelector('.modal-header') as HTMLElement).style.backgroundColor = '#19ff00';
            (this.Templet.querySelector('.modal-content') as HTMLElement).style.border = '4px solid rgb(25, 255, 0)';
        }
        else if (Type == 'Warning') {
            this.Templet.querySelector('.modal-title').innerHTML = 'Warning';
            (this.Templet.querySelector('.modal-header') as HTMLElement).style.backgroundColor = 'rgba(255, 138, 0, 0.93)';
            (this.Templet.querySelector('.modal-content') as HTMLElement).style.border = '4px solid rgb(255, 146, 18)';
        }
        else {
            this.Templet.querySelector('.modal-title').innerHTML = 'Error';
            (this.Templet.querySelector('.modal-header') as HTMLElement).style.backgroundColor = '#dc3545';
            (this.Templet.querySelector('.modal-content') as HTMLElement).style.border = '4px solid #dc3545';
        }
        // if (Message.length < 70) {
        //     this.Templet.querySelector('.modal-dialog').classList.add('modal-sm');
        // }

        this.Templet.style.display = "block";
        await this.delay(5);
        this.Templet.classList.add('show');
        document.body.insertAdjacentHTML('beforeend', `<div class="modal-backdrop fade show" id="backdrop-${this.Templet.id}"></div>`);
        (this.Templet.querySelector(".modal-header") as HTMLElement).style.cursor = "default";
        vModal.dragElement(this.Templet, '.modal-header');
        var zIndex = parseInt(vModal.findHighestZIndex('div'));
        (document.querySelector(`#backdrop-${this.Templet.id}`) as HTMLElement).style.zIndex = (++zIndex).toString();
        this.Templet.style.zIndex = ((++zIndex) + 2003).toString();
    }
    async Confirm(Message, OnOK, OnCancel, data = undefined) {
        this.Templet = this.getTemplate(Templates.Confirm);
        document.body.appendChild(this.Templet);
        (this.Templet.querySelector('.modal-header') as HTMLElement).style.backgroundColor = '#19ff00';
        (this.Templet.querySelector('.modal-content') as HTMLElement).style.border = '4px solid rgb(25, 255, 0)';

        this.addEvent('.closeModal', 'click', this.Close, OnCancel, data);
        this.addEvent('.Yes', 'click', this.Close, OnOK, data);
        this.Templet.querySelector('.modal-body').innerHTML = Message;
        // if (Message.length < 70) {
        //     this.Templet.querySelector('.modal-dialog').classList.add('modal-sm');
        // }
        this.Templet.style.display = "block";
        await this.delay(5);
        this.Templet.classList.add('show');

        document.body.insertAdjacentHTML('beforeend', `<div class="modal-backdrop fade show" id="backdrop-${this.Templet.id}"></div>`);
        (this.Templet.querySelector(".modal-header") as HTMLElement).style.cursor = "default";
        vModal.dragElement(this.Templet, '.modal-header');
        var zIndex = parseInt(vModal.findHighestZIndex('div'));
        (document.querySelector(`#backdrop-${this.Templet.id}`) as HTMLElement).style.zIndex = (++zIndex).toString();
        this.Templet.style.zIndex = ((++zIndex) + 2003).toString();
    }
    async PopUp(Content, Title, OnClose, data = undefined, showFooter = true) {
        this.Templet = this.getTemplate(Templates.PopUp);
        if (!showFooter) {
            this.Templet.querySelector('.modal-footer').remove();
        }
        document.body.appendChild(this.Templet);
        this.addEvent('.closeModal', 'click', this.Close, OnClose, data);
        this.Templet.querySelector('.modal-body').innerHTML = Content;
        this.Templet.querySelector('.modal-title').innerHTML = Title;

        this.Templet.style.display = "block";
        await this.delay(5);
        this.Templet.classList.add('show');
        document.body.insertAdjacentHTML('beforeend', `<div class="modal-backdrop fade show" id="backdrop-${this.Templet.id}"></div>`);
        (this.Templet.querySelector(".modal-header") as HTMLElement).style.cursor = "default";
    }
    getTemplate(Name: string): HTMLDivElement {
        let element = document.createElement('div');
        element.id = Name + "_" + (new Date()).getTime();
        element.classList.add('modal', 'fade', 'vikas-modal');
        element.setAttribute("role", "dialog");
        switch (Name) {
            case Templates.Alert:
                element.innerHTML = this.AlertTemplet();
                break;
            case Templates.Confirm:
                element.innerHTML = this.ConfirmTemplet();
                break;
            case Templates.PopUp:
                element.innerHTML = this.PopUpTemplet();
                break;

            default:
                break;
        }

        return element;
    }
    AlertTemplet(): string {
        return `<div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title text-center">Error !</h4>
                    <button type="button" class="close closeModal  no-outline">&times; </button>
                    </div>
                <div class="modal-body">
                    Some text in the modal.
                </div>
            <div class ="modal-footer">
                    <button type="button" class ="btn btn-default closeModal">Close</button>
                </div>
            </div>
        </div>`;
    }

    ConfirmTemplet(): string {
        return `<div class="modal-dialog">
            <div class="modal-content" style="border: 4px solid rgb(25, 255, 0)">
                <div class="modal-header">
                <h4 class="modal-title text-center">Confirm</h4>
                 <button type="button" class ="close closeModal">&times; </button>
                 </div>
                <div class="modal-body">
                    Some text in the modal.
                </div> <div class="modal-footer">
                    <button type="button" class ="btn btn-success Yes">Yes</button>
                    <button type="button" class ="btn btn-danger closeModal">No</button>
                </div>
            </div>
        </div>`;
    }
    PopUpTemplet(): string {
        return `<div class="modal-dialog">
            <div class ="modal-content" style="border-radius:0;border: 5px solid rgb(48, 167, 255);">
                <div class ="modal-header" style="background-color:rgb(48, 167, 255);border-radius: 0;color:#fff;">
                <h4 class="modal-title text-center">Modal !</h4>
                <button type="button" class ="close closeModal" style="color:#fff;">&times; </button>
                </div>
                <div class ="modal-body scroll">
                    Some text in the modal.
                </div> <div class="modal-footer">
                    <button type="button" class ="btn btn-default closeModal">Close</button>
                </div>
            </div>
        </div>`;
    }
    Close(_this: this) {
        if (typeof _this.Templet != 'undefined') {
            _this.Templet.classList.remove('show');
            _this.Templet.style.display = "none";
            var backdropID = '#backdrop-' + _this.Templet.id;
            document.querySelector(backdropID).remove();
            if (!_this.xyz) {
                _this.Templet.remove();
            }
        }
    }
    addEvent(Obj, event, Fun, callback, data) {
        let _this = this;
        if (typeof Obj != 'undefined' && typeof event != 'undefined' && typeof Fun != 'undefined' && event != '') {
            var els = _this.Templet.querySelectorAll(Obj);
            for (let index = 0; index < els.length; index++) {
                if (data) {
                    (els[index] as HTMLElement).dataset.data = JSON.stringify(data);
                }
                (els[index] as HTMLElement).onclick = function (ev) {
                    Fun(_this);
                    if (typeof callback != 'undefined') {
                        var ds = undefined;
                        if ((ev.currentTarget as HTMLElement).dataset.data) {
                            ds = JSON.parse((ev.currentTarget as HTMLElement).dataset.data);
                        }
                        callback(ds);
                    }
                }
            }
        }
    }
    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}

export enum Templates {
    Alert = 'Alert',
    Confirm = 'Confirm',
    PopUp = 'PopUp',
}

export class vModal {
    static Success(msg, OnClose, data = undefined) {
        let Modal = new VikasModal();
        Modal.Alert(msg, "Success", OnClose, data);
    }
    static Warning(msg, OnClose, data = undefined) {
        let Modal = new VikasModal();
        Modal.Alert(msg, "Warning", OnClose, data);
    }
    static Error(msg, OnClose, data = undefined) {
        let Modal = new VikasModal();
        Modal.Alert(msg, "Error", OnClose, data);
    }
    static Confirm(msg, OnOK, OnCancel, data = undefined) {
        let Modal = new VikasModal();
        Modal.Confirm(msg, OnOK, OnCancel, data);
    }
    static PopUp(Content, Title, OnClose, showFooter = true) {
        let Modal = new VikasModal();
        Modal.PopUp(Content, Title, OnClose, undefined, showFooter);
    }

    static async Modal(selector, OnClose = undefined, data = undefined) {
        let Modal = new VikasModal();
        var template = document.querySelector(selector);
        var zIndex = parseInt(vModal.findHighestZIndex('div'));
        Modal.Templet = template;
        Modal.xyz = true;
        Modal.addEvent('.closeModal', 'click', Modal.Close, OnClose, data)

        template.style.display = "block";
        await Modal.delay(5);
        template.classList.add('show');
        document.body.insertAdjacentHTML('beforeend', `<div class="modal-backdrop fade show" id="backdrop-${template.id || (new Date()).getTime()}" style="z-index:${(++zIndex).toString()}"></div>`);
        (template.querySelector(".modal-header") as HTMLElement).style.cursor = "default";
        vModal.dragElement(template, '.modal-header');
        template.style.zIndex = ((++zIndex) + 2003).toString();
    }
    static CloseModal(selector) {
        let Modal = new VikasModal();
        Modal.Templet = document.querySelector(selector);
        Modal.xyz = true;
        Modal.Close(Modal);
    }

    static dragElement(elmnt, handle) {
        var element = elmnt;
        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (element.querySelector(handle)) {
            /* if present, the header is where you move the DIV from:*/
            element.querySelector(handle).onmousedown = dragMouseDown;
        } else {
            /* otherwise, move the DIV from anywhere inside the DIV:*/
            element.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            var modalDialog = element.querySelector('.modal-dialog') as HTMLElement;
            if (modalDialog != null) {
                var computedStyle = getComputedStyle(modalDialog);
                var topm = parseInt(computedStyle.marginTop.replace('px', ''))
                var bottom = parseInt(computedStyle.marginBottom.replace('px', ''))
                var left = parseInt(computedStyle.marginLeft.replace('px', ''))
                var right = parseInt(computedStyle.marginRight.replace('px', ''))
                if (-topm >= (element.offsetTop - pos2) && (element.offsetTop - pos2) != 0) {
                    return;
                }
                if (-left >= (element.offsetLeft - pos1) && (element.offsetLeft - pos1) != 0) {
                    return;
                }
                if ((document.body.clientHeight - (topm + modalDialog.clientHeight)) <= (element.offsetTop - pos2) && (element.offsetTop - pos2) != 0) {
                    return;
                }
                if (left <= (element.offsetLeft - pos1) && (element.offsetLeft - pos1) != 0) {
                    return;
                }

            }
            element.style.top = (element.offsetTop - pos2) + "px";
            element.style.left = (element.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            /* stop moving when mouse button is released:*/
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }
    static findHighestZIndex(elem) {
        // var elems = document.getElementsByTagName(elem);
        // var highest = "0";
        // for (var i = 0; i < elems.length; i++) {
        //     var zindex = document.defaultView.getComputedStyle(elems[i], null).getPropertyValue("z-index");
        //     if ((zindex > highest) && (zindex != 'auto')) {
        //         highest = zindex;
        //     }
        // }
        // return highest;
        var elems = document.getElementsByTagName('*');
        var arr = Array.from(elems);
        var highest = "0";
        arr.forEach(element => {
            var zindex = document.defaultView.getComputedStyle(element as Element, null).getPropertyValue("z-index");
            if ((zindex > highest) && (zindex != 'auto')) {
                highest = zindex;
            }
        });
        return highest;
    }
}

window.onload = function () {debugger;
    eval('window.vModal=vModal');
}


