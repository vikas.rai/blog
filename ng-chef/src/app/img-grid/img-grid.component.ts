import { Component, OnInit, Input } from '@angular/core';
import { common } from '../common-model';

@Component({
  selector: 'app-img-grid',
  templateUrl: './img-grid.component.html',
  styleUrls: ['./img-grid.component.css']
})
export class ImgGridComponent implements OnInit {

  constructor() { }
  apiUrl = common.ApiURL;
  @Input() data: any[];
  ngOnInit() {
  }

}
