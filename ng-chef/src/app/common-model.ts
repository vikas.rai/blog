import { vModal } from './vikas-modal';
declare var $: any;
export class common {
    static ApiURL = 'https://pastrychefrahul.com/api/';
    //static ApiURL = 'http://localhost:62890/api/';
    static OpenSubscribe(showAlert: boolean = false) {
        if (common.Cookie('userid') == null) {
            common.RemoveCookie('name');
            common.RemoveCookie('userid');
            common.RemoveCookie('email');
        }
        if (common.Cookie('name') != null && common.Cookie('email') != null) {
            if (showAlert)
                vModal.Success('Already Subscribed.', undefined);
            return;
        }
        var content = `
       <div class="subscribe-container">
       <form>
       <div class="form-group">
       <input class="form-control name" type="text" placeholder="Enter Name Here..."/>
       <span class="text-danger"></span>
       </div>
       <div class="form-group">
       <input class="form-control email" type="email" placeholder="Enter Emailaddress Here..."/>
       <span class="text-danger"></span>
       </div>
       <hr>
       <div class="text-center">
       <button class="btn btn-primary btn-subscribe" type="button">Submit</button>
       </div>
       </form>
       </div>
       `;
        vModal.PopUp(content, 'Subscribe', undefined, false);
        setTimeout(() => {
            var popUp = $('.subscribe-container').parents('.vikas-modal');
            popUp.find('.closeModal').remove();
            $('#backdrop-' + popUp.attr('id')).css('opacity', '0.95');
            $('.btn-subscribe').click(function () {
                var $name = $('input.name');
                var $email = $('input.email');
                var isvalid = true;
                if ($name.val().trim() == '') {
                    $name.parents('div.form-group').find('span').html('please enter your name.');
                    var isvalid = false;
                }
                else {
                    $name.parents('div.form-group').find('span').empty();
                }
                if ($email.val().trim() == '') {
                    $email.parents('div.form-group').find('span').html('please enter your emailaddress.');
                    var isvalid = false;
                }
                else if (!common.validateEmail($email.val().trim())) {
                    $email.parents('div.form-group').find('span').html('please enter valid emailaddress.');
                    var isvalid = false;
                }
                else {
                    $email.parents('div.form-group').find('span').empty();
                }
                var modalid = $(this).parents('.vikas-modal').attr('id');
                if (isvalid) {
                    debugger;
                    var data = `?Name=${$name.val()}&Email=${$email.val()}`
                    $('.btn-subscribe').html('<i class="fa fa-spinner fa-spin"></i> Submit');
                    $('.btn-subscribe').attr('disabled', true);
                    $.ajax({
                        type: "GET",
                        url: common.ApiURL + "File/Subscribe" + data,
                        //data: Params,
                        success: function (data) {
                            $('.btn-subscribe').html('Submit');
                            $('.btn-subscribe').attr('disabled', false);
                            common.SetCookie('name', $name.val(), (100 * 365 * 24));
                            common.SetCookie('userid', data, (100 * 365 * 24));
                            common.SetCookie('email', $email.val(), (100 * 365 * 24));
                            vModal.CloseModal('#' + modalid);
                            vModal.Success('subscribed successfully', undefined);
                        },
                        error: function (e) {
                            console.log(e);
                            $(event.target).attr('disabled', true);
                            $(event.target).html('<i class="fa fa-refresh fa-spin"></i>');
                            $('.btn-subscribe').html('Submit');
                            $('.btn-subscribe').attr('disabled', false);
                        }
                    });
                    return false;

                }
            })
        }, 500);
    }
    static ToQueryString(json) {
        return '?' + 
            Object.keys(json).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(json[key]);
            }).join('&');
    }
    static validateEmail($email): boolean {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }
    static SetCookie(key, val, expHour) {
        if (expHour == undefined) {
            expHour = 24;
        }
        var exp = new Date();
        exp.setTime(exp.getTime() + expHour * 60 * 60 * 1000);
        document.cookie = key + '=' + val + ';expires=' + exp.toUTCString();
    }
    static Cookie(key) {
        var val = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return val ? val[2] : null;
    }
    static RemoveCookie(key) {
        var exp = new Date();
        exp.setTime(exp.getTime() + (-60 * 60 * 1000));
        document.cookie = key + '=' + undefined + ';expires=' + exp.toUTCString();
    }
    static uploadImage1(Editor) {
        var fileInput = $('<input type="file"/>');
        fileInput.change(function () {
            if (this.files && this.files[0]) {
                var FR = new FileReader();
                FR.addEventListener("load", function (e: any) {
                    Editor.insertHtml('<img src="' + e.target.result + '">');
                });
                FR.readAsDataURL(this.files[0]);
            }
        });
        fileInput.click();
    }
    static uploadImage(Editor) {
        var fileInput = $('<input type="file"/>');
        fileInput.change(function () {
            if (this.files && this.files[0]) {
                common.UploadFile(this.files[0], function (data) {
                    Editor.insertHtml('<img src="' + common.ApiURL + "/File/Download?path=" + data + '">');
                });
            }
        });
        fileInput.click();
    }
    static UploadFile(file, callback) {
        var data = new FormData();
        data.append("Name", "vikas");
        data.append("file", file);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: common.ApiURL + "File/Upload",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (callback) callback(data);
            },
            error: function (e) {
                console.log(e);
            }
        });

    }
    static GetMedia(callback) {
        var data = {};
        $.ajax({
            type: "GET",
            url: common.ApiURL + "File/GetMedia",
            data: data,
            success: function (data) {
                var content = $('<div>');
                content.append($('<div class="row media-modal">'));
                var imgUpload = `<div class="col-sm-12"><input type="file" style="width:100px"/>
                <button class="btn btn-primary" onclick="Common.changeFile($(this).siblings('input[type=file]')[0])">Upload</button></div><br><br>`;
                content.find('.row').append(imgUpload);
                if (data.length > 0) {
                    for (let index = 0; index < data.length; index++) {
                        var img = `
                      
                       <div class="col-sm-2"> <img onclick="Common.SelectImg(this)" title="${data[index].FileName}" style="width:100%" src="${common.ApiURL}/File/Download?path=${data[index].FilePath}"/></div>`;
                        content.find('.row').append(img);
                    }
                    //vModal.CloseModal('#' + $('.media-modal').parents('.modal').attr('id'));
                    var Uploadbtn = `<div class="col-sm-12">
                    <button class="btn btn-primary" onclick="Common.InsertImgInEditor($(this).siblings('input[type=file]')[0])">Inser in editor</button></div><br><br>`;
                    content.find('.row').append(Uploadbtn);
                    vModal.PopUp(content.html(), "Media Gallary", undefined, false);
                    $('.media-modal').parents('.modal').find('.modal-dialog').addClass('modal-lg');
                }
                else {
                    content.find('.row').append("No Media Found");
                    vModal.PopUp(content.html(), "Media Gallary", undefined, false);
                }

            },
            error: function (e) {
                console.log(e);
            }
        });

    }
    static SelectImg(thisObj) {
        $('.media-modal img').removeClass('active');
        $(thisObj).addClass('active');
    }
    static InsertImgInEditor(thisObj) {
        eval(`CKEDITOR.instances.blogEditor.insertHtml('<img src="' +  $('.media-modal img.active').attr('src') + '">')`);
        $('#MediaGallaryPopup').modal('hide');
    }
    static changeFile(thisObj) {
        if (thisObj.files && thisObj.files[0]) {
            common.UploadFile(thisObj.files[0], function () {
                common.GetMedia(undefined);
            });
        }
    }
    static loadScript(Url, callback = undefined) {
        let body = <HTMLDivElement>document.body;
        let script = document.createElement('script');
        script.innerHTML = '';
        script.src = Url;
        script.async = true;
        script.defer = true;
        script.onload = callback;
        body.appendChild(script);
    }
    static RecipeTemplate() {
        var RecipeTemplate = `
        <section class="ls s-py-75 s-py-lg-100 c-gutter-60 blog-page">
        <div class="container">
        <div class="row">
        <article class="vertical-item content-padding post type-post status-publish format-standard has-post-thumbnail bordered"><!-- .post-thumbnail -->
        <div class="item-media post-thumbnail"><img alt="" src="assets/images/gallery-09.jpg" />
        <div class="bg-dark-transpatent entry-meta small-text text-md-left">Posted on - <a href="" rel="bookmark"> 25 Apr, 2020</a></div>
        </div>
        
        <h2><span style="color:#e67e22">&nbsp; &nbsp;Recipe Name</span></h2>
        
        <div class="item-content"><!-- .entry-meta -->
        <div class="entry-content">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed augue tristique purus euismod condimentum. Donec fermentum lacinia est nec facilisis. Praesent ut lectus imperdiet, rhoncus diam quis, sodales turpis. Sed in fringilla nunc. Duis vel auctor libero. Duis non lectus pulvinar, fringilla purus et, vestibulum neque.</p>
        
        <p class="small-margin">Praesent varius nibh mattis ipsum varius porta. In sit amet semper orci. Mauris gravida nisi in finibus finibus. Aenean ultrices arcu et rutrum venenatis. Curabitur rhoncus, ante quis ullamcorper posuere, leo metus dapibus lacus, a lobortis lectus diam et turpis. Pellentesque a pulvinar quam. Mauris feugiat sapien pellentesque.</p>
        
        <p>Etiam sollicitudin, ipsum egestas lacinia sagittis, risus leo placerat lacus, at malesuada nunc sem vel erat. Pellentesque elementum efficitur risus in molestie. Cras facilisis dui felis. Nulla vel metus et quam mollis.</p>
        
        <ul>
            <li>Aliquam id nisi id mauris</li>
            <li>Maecenas sit amet ante non</li>
            <li>Aenean vel libero eu nisle</li>
            <li>Duis viverra eros viverra</li>
        </ul>
        
        <p><img alt="" class="alignleft bordered" src="assets/images/images-about.jpg" /></p>
        
        <p class="small-margin">Donec sit amet elit ac felis efficitur scelerisque nec lacinia mauris. Praesent aliquam sed ante non euismod. In ac quam facilisis nibh pulvinar faucibus.</p>
        
        <p class="small-margin">At vulputate metus. Proin consectetur condimentum gravida. Ut sagittis lectus urna, quis molestie ante condimentum vel. Duis eget ipsum massa.</p>
        
        <p class="small-margin">Integer elementum erat metus, non egestas nisl suscipit ac. Ut consequat tincidunt ullamcorper. Nam scelerisque, lectus ac mollis aliquet, massa tellus viverra arcu, nec lobortis nunc nunc sed purus phasellus eget.</p>
        
        <div class="d-lg-block d-none divider-35">&nbsp;</div>
        
        <p>Quisque vel justo in nisl tempor vulputate in accumsan nibh. Curabitur accumsan mauris in hendrerit fringilla. Phasellus ut gravida augue. Etiam aliquam pretium erat vehicula convallis. Phasellus rutrum ac lectus at eleifend. Nullam pellentesque ultrices justo, ut eleifend velit sollicitudin a.x</p>
        
        <p>&nbsp;</p>
        </div>
        <!-- .entry-content --></div>
        <!-- .item-content --></article>
        
        <div class="author-bio bordered content-padding ls side-item">
        <div class="row">
        <div class="col-lg-6 col-md-6 col-xl-8">
        <div class="item-content">
        <div>&nbsp;</div>
        
        <h3>Chef Rahul Rai</h3>
        
        <p>Pellentesque at tempor tortor. Vestibulum sed semper arcu. Curabitur sodales in arcu efficitur egestas. Aenean urna augue, placerat consequat varius id.</p>
        
        <div class="author-social"><span style="font-size:16px"><a class="fa fa-facebook" href="https://www.facebook.com/profile.php?id=100009823686190">fa-facebook</a> <a class="fa fa-twitter" href="https://twitter.com/chefRahulrai1">fa-twitter</a> <a class="fa fa-instagram" href="https://www.instagram.com/rahulrai5119">fa-insta</a> <a class="fa fa-youtube-play" href="https://www.youtube.com/channel/UC64kwpw23r3bxEGarSoKAiA">fa-youtube-play</a></span></div>
        </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-xl-4">
        <div class="cover-image item-media"><img alt="" src="assets/images/images-about.jpg" /></div>
        </div>
        </div>
        </div>
        
        <div class="d-lg-block d-none divider-60">&nbsp;</div>
        </div>
        </div>
        </section>
        `
        return RecipeTemplate;
    }
}

window.onload = function () {
    eval('window.Common=common');
}