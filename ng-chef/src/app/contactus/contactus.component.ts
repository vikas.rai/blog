import { Component, OnInit } from '@angular/core';
import { common } from '../common-model';
import { vModal } from '../vikas-modal';
declare var $: any;
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.submitContact();
  }
  submitContact() {
    $('form.contactForm').off('submit');
    $('form.contactForm').submit(function () {
      var f = $(this).find('.form-group'),
        ferror = false,
        emailExp = /^[^\s()<>@,;:\/]+@\w[\w\.-]+\.[a-z]{2,}$/i;

      f.children('input').each(function () { // run all inputs

        var i = $(this); // current input
        var rule = i.attr('data-rule');

        if (rule !== undefined) {
          var ierror = false; // error flag for current input
          var pos = rule.indexOf(':', 0);
          if (pos >= 0) {
            var exp = rule.substr(pos + 1, rule.length);
            rule = rule.substr(0, pos);
          } else {
            rule = rule.substr(pos + 1, rule.length);
          }

          switch (rule) {
            case 'required':
              if (i.val() === '') {
                ferror = ierror = true;
              }
              break;

            case 'minlen':
              if (i.val().length < parseInt(exp)) {
                ferror = ierror = true;
              }
              break;

            case 'email':
              if (!emailExp.test(i.val())) {
                ferror = ierror = true;
              }
              break;
            case 'mobile':
              var mobileNumber = parseInt(i.val())
              if (!mobileNumber || mobileNumber.toString().length != 10) {
                ferror = ierror = true;
              }
              break;

            case 'checked':
              if (!i.is(':checked')) {
                ferror = ierror = true;
              }
              break;

            case 'regexp':
              exp = new RegExp(exp);
              if (!exp.test(i.val())) {
                ferror = ierror = true;
              }
              break;
          }
          i.next('.validation').html((ierror ? (i.attr('data-msg') !== undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
        }
      });
      f.children('textarea').each(function () { // run all inputs

        var i = $(this); // current input
        var rule = i.attr('data-rule');

        if (rule !== undefined) {
          var ierror = false; // error flag for current input
          var pos = rule.indexOf(':', 0);
          if (pos >= 0) {
            var exp = rule.substr(pos + 1, rule.length);
            rule = rule.substr(0, pos);
          } else {
            rule = rule.substr(pos + 1, rule.length);
          }

          switch (rule) {
            case 'required':
              if (i.val() === '') {
                ferror = ierror = true;
              }
              break;

            case 'minlen':
              if (i.val().length < parseInt(exp)) {
                ferror = ierror = true;
              }
              break;
          }
          i.next('.validation').html((ierror ? (i.attr('data-msg') != undefined ? i.attr('data-msg') : 'wrong Input') : '')).show('blind');
        }
      });
      if (ferror) return false;
      else var str = $(this).serialize();
      var data = `?Name=${$('#name').val()}&Email=${$('#email').val()}&Mobile=${$('#mobile').val()}&Message=${$('#message').val()}`
      $('#btn-submit').html('<i class="fa fa-spinner fa-spin"></i> Submit');
      $('#btn-submit').attr('disabled', true);
      $.ajax({
        type: "GET",
        url: common.ApiURL + "File/SendEmail" + data,
        //data: Params,
        success: function (data) {
          $('#btn-submit').html('Submit');
          $('#btn-submit').attr('disabled', false);
          vModal.Success("Submitted successfully, we will contact you soon.", function () {

          })

        },
        error: function (e) {
          console.log(e);
          $(event.target).attr('disabled', true);
          $(event.target).html('<i class="fa fa-refresh fa-spin"></i>');
        }
      });
      return false;
    });

  }

}
