import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { common } from '../common-model';
import { Router } from '@angular/router';
import { vModal } from '../vikas-modal';
declare var $: any;

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  PageSize: number = 6;
  PageNo: number = 0;
  PageCount: number = 0;
  Search: string = "";
  lstRecipe: any = [];

  constructor(public aa: ChangeDetectorRef, private router: Router) {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 500);
  }
  isLoggedIN = false;
  ngOnInit() {
    this.isLoggedIN = Boolean(common.Cookie("isLoggedIN"))
    eval('window.self1=this');
    this.bindRecipe();
  }
  pageClick(event) {
    this.PageNo = parseInt(event.currentTarget.dataset.index);
    this.bindRecipe();
  }
  SearchClick() {
    this.Search = $('#txtSearch').val();
    this.PageNo = 0;
    this.PageCount = 0;
    this.bindRecipe();
  }
  ClearSearchClick() {
    $('#txtSearch').val('');
    this.PageNo = 0;
    this.PageCount = 0;
    this.Search = "";
    this.bindRecipe();
  }
  ClearTextClick() {
    $('#txtSearch').val('');
  }
  CreateRecipe() {
    localStorage.removeItem('BlogID');
    $('#txtRecipe').data('id', undefined);
    this.router.navigateByUrl('/Blog/Create');
  }
  bindRecipe() {
    eval('window.self1.lstRecipe=null');
    eval('window.self1.aa.detectChanges();');
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/Recipes?PageOffset=" + (this.PageNo * this.PageSize) + "&PageSize=" + this.PageSize + "&Search=" + this.Search,
      data: data,
      success: function (data) {
        
        eval('window.self1.lstRecipe=data');
        if (data.length > 0) {
          eval('window.self1.PageCount=Math.ceil(data[0].TotalCount/window.self1.PageSize)');
        }
        else {
          eval('window.self1.PageCount=0');
        }
        eval('window.self1.aa.detectChanges();');
      },
      error: function (e) {
        console.log(e);
      }
    });
  }
  deleteRecipe(ID) {
    vModal.Confirm("Are you want to delete this?", function (ID) {
      var data = {};
      $.ajax({
        type: "GET",
        url: common.ApiURL + "File/DeleteRecipes?ID=" + ID,
        data: data,
        success: function (data) {
          if (data) {
            eval(`
            var item=window.self1.lstRecipe.filter(function(item){return item.ID==${ID}})[0]
            var index=window.self1.lstRecipe.indexOf(item)
            window.self1.lstRecipe.splice(index, 1);
            if(window.self1.lstRecipe.length>0){
              window.self1.PageCount=Math.ceil(window.self1.lstRecipe[0].TotalCount/window.self1.PageSize);
            }
            else{
              window.self1.bindRecipe();
            }
            `);
            eval('window.self1.aa.detectChanges();');
          }

        },
        error: function (e) {
          console.log(e);
        }
      });
    }, undefined, ID)
  }
}
