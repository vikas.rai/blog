import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { common } from '../common-model';
declare var $: any;

@Component({
  selector: 'app-recipe-template',
  templateUrl: './recipe-template.component.html',
  styleUrls: ['./recipe-template.component.css']
})
export class RecipeTemplateComponent implements OnInit {

  constructor(private router: Router) { }
  @Output() DeleteEvent = new EventEmitter<string>();
  @Input() data: any[];
  @Input() recipePage: boolean;
  isLoggedIN = false;
  ngOnInit() {
    this.isLoggedIN = Boolean(common.Cookie("isLoggedIN"))
  }
  edit(id) {
    localStorage.BlogID = id;
    this.router.navigateByUrl('/Blog/Create');
  }
  delete(id) {
    this.DeleteEvent.next(id);

  }
}
