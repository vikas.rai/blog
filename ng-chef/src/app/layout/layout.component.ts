import { Component, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { common } from '../common-model';
import { Title } from '@angular/platform-browser';
declare var windowLoadInit: any;
declare var documentReadyInit: any;
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  isHome = true;
  isLoggedIN = false;
  UserName = '';
  constructor(private router: Router, private titleService: Title) {

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHome = event.url == '/Home' || event.url == '/' || event.url == 'Blog' || event.url == '/Blog/Create'
        if (event.url.indexOf('Blog') > 0) {
          localStorage.isreload = false;
        }
        if (!this.isHome) {
          setTimeout(() => {
            var el = document.getElementsByClassName('page_header_wrapper')[0] as HTMLElement;
            el.classList.add('clsheader');
          }, 200);

        }
        windowLoadInit();
        var hash: any = location.hash;
        this.titleService.setTitle('Chef Rahul' + hash.replaceAll('#/', ' | ').replaceAll('/', ' | '));
      }
      if (event instanceof NavigationStart) {

        if (localStorage.isreload == 'true') {
          localStorage.isreload = false;

        }
        else {
          localStorage.isreload = true;
        }

        if (event.url.indexOf('Blog') > 0) {
          //localStorage.isreload = true;
        }
        else {
          localStorage.isreload = false;
        }
      }
    });
  }
  ngOnInit() {
    documentReadyInit();
    common.OpenSubscribe();
    this.isLoggedIN = Boolean(common.Cookie("isLoggedIN"));
    this.UserName = common.Cookie("UserName");
    //windowLoadInit();
  }
  logout() {
    common.RemoveCookie("isLoggedIN");
    common.RemoveCookie("UserName");
    location.href = location.origin + "/#/Login"
  }

  @HostListener('window:popstate')
  footerResize() {
    console.log(this);
  }
}
