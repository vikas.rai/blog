import { Component, OnInit } from '@angular/core';
import { common } from '../common-model';
declare var $: any;

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {

  constructor() { }
  ngOnInit() {
    this.GetSubscribers();
  }
  GetSubscribers() {
    $('.btn-subscribe').html('<i class="fa fa-spinner fa-spin"></i> Submit');
    $('.btn-subscribe').attr('disabled', true);
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/GetSubscribe",
      success: function (data) {
        $('#spnSubscribers').html(data);
      },
      error: function (e) {
      }
    });
  }
}
