import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { common } from '../common-model';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { vModal } from '../vikas-modal';
declare var $: any;

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  recipeUrl = "";
  OverAllRating: any = 0;
  RecipeID = 0;
  data: any = null;
  Replies: any = [];
  rating = 5;
  selectedRating = 0;
  displayRating = 0;
  ratingDescription = ['Hated it', "Didn't like it", 'Just OK', 'Liked it', 'Loved it']
  constructor(private router: Router, public aa: ChangeDetectorRef, private titleService: Title, private meta: Meta) {


    if (localStorage.isreload == 'true') {
      location.reload();
    }
  }
  ngOnInit() {
    eval('window.self1=this');
    var arr = this.router.url.split('/');
    this.RecipeID = parseInt(arr[arr.length - 1]);
    this.recipeUrl = encodeURIComponent("https://pastrychefrahul.com/#/Blog/" + this.RecipeID);
    this.recipeUrl = "https://api.whatsapp.com/send?text=" + this.recipeUrl;

    this.GetRecipe();
  }
  getDate() {
    var date = new Date();
    var day = date.getDate();
    var month = date.toLocaleString('default', { month: 'short' });
    var year = date.getFullYear();

    return day + ' ' + month + ' ' + year + ', ' + this.formatAMPM(date);
  }
  formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  reply() {
    if (this.selectedRating == 0) {
      alert('select rating');
      return;
    }
    if ($('#txtreply').val().trim() == '') {
      alert('type something in comment box.');
      return;
    }
    if ($('#txtreply').val().trim().length > 500) {
      alert('comment should be less then 500 chars.');
      return;
    }
    var params: any = {};
    params.Rating = this.selectedRating;
    params.RecipeID = this.RecipeID;
    params.Comment = $('#txtreply').val();
    params.CreatedDate = this.getDate();
    params.UserID = common.Cookie('userid');
    params.Name = common.Cookie('name');
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/RecipeComments" + common.ToQueryString(params),
      data: params,
      success: function (data) {
        // vModal.Success("Reply posted.", undefined);
        $('#txtreply').val('');
        eval('window.self1.Replies.unshift(params);');

        eval('window.self1.aa.detectChanges();');
      },
      error: function (e) {
        console.log(e);
      }
    });
  }
  ratingHover(ev) {
    var CurrSpan = ev.currentTarget as HTMLSpanElement;
    var index = CurrSpan.dataset.index;
    this.displayRating = ev.type == 'mouseenter' ? parseInt(index) + 1 : this.selectedRating;
  }
  ratingClick(ev) {
    var CurrSpan = ev.currentTarget as HTMLSpanElement;
    var index = CurrSpan.dataset.index;
    this.selectedRating = parseInt(index) + 1;
    this.displayRating = this.selectedRating;
  }
  GetRecipe() {
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/Recipes?ID=" + this.RecipeID,
      data: data,
      success: function (data) {
        var Comments = data.Comments;
        var rating = Comments.length == 0 ? 0 : Comments[0].Rating;

        if (Comments.length > 0) {
          var w = Comments.filter(function (item) {
            if (item.UserID == common.Cookie('userid')) {
              return true;
            }
            else {
              return false;
            }
          })
          rating = w.length == 0 ? 0 : w[0].Rating;
        }
        else {
          rating = 0;
        }

        eval('window.self1.selectedRating = rating;window.self1.displayRating = rating;window.self1.Replies=Comments;');
        $('.blogDataaa').html(data.Contents);
        eval('window.self1.meta.updateTag({ name: "og:url", content: "https://pastrychefrahul.com/#/Blog/' + data.ID + '" });');
        eval('window.self1.meta.updateTag({ name: "twitter:url", content: "https://pastrychefrahul.com/#/Blog/' + data.ID + '" });');
        eval('window.self1.meta.updateTag({ name: "description", content: "' + data.Name + '" });window.self1.OverAllRating="' + data.Rating + '";');
        eval('window.self1.titleService.setTitle("Chef Rahul | ' + data.Name + '");');
        eval('window.self1.aa.detectChanges();');
      },
      error: function (e) {
        console.log(e);
      }
    });
  }

}
