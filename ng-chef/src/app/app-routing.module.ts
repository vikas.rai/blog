import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { TipsComponent } from './tips/tips.component';
import { BlogComponent } from './blog/blog.component';
import { CreateBlogComponent } from './create-blog/create-blog.component';
import { LayoutComponent } from './layout/layout.component';
import { RecipesComponent } from './recipes/recipes.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [
  { path: 'Blog/Create', component: CreateBlogComponent },
  { path: 'Login', component: LoginComponent },

  {
    path: '', component: LayoutComponent,
    //pathMatch: 'full',
    children: [
      {
        path: '',
        redirectTo: '/Home',
        pathMatch: 'full'
      },
      { path: 'Home', component: HomeComponent },
      { path: 'About', component: AboutusComponent },
      { path: 'Contact', component: ContactusComponent },
      { path: 'Tips', component: TipsComponent },
      { path: 'Recipes', component: RecipesComponent },
      { path: 'Blog/:id', component: BlogComponent },
      { path: '**', component: NotfoundComponent }]
  },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
