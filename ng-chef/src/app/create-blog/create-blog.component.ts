import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { common } from '../common-model';
import { vModal } from '../vikas-modal';
//declare var ClassicEditor: any;
declare var CKEDITOR: any;
declare var $: any;
@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  editor = null;
  lstMedia: any[] = null;
  PageSize: number = 10;
  PageNo: number = 0;
  PageCount: number = 0;
  Search: string = "";
  constructor(public aa: ChangeDetectorRef) {
    //aa.detectChanges();
  }

  ngOnInit() {
    eval('window.createBlog=this');
    common.loadScript("assets/ckeditor/ckeditor.js", this.ckEditor);

  }
  ckEditor() {
    CKEDITOR.tools.enableHtml5Elements(document);
    CKEDITOR.replace(document.querySelector('#blogEditor'), {
      removeButtons: `Image,Print,NewPage,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,IFrame`,
      //extraAllowedContent: '- img[!src,alt,width,height];*(*);- section;article;div',
      allowedContent: true
    });
    CKEDITOR.instances.blogEditor.ui.addButton('AddCustomImage', {
      label: 'upload image',
      command: 'AddCustomImage',
      // toolbar: 'insert',
      icon: '/assets/images/upload-image-icon.png',
      className: "customimagebutton"
    });
    CKEDITOR.instances.blogEditor.ui.addButton('AddImageGallary', {
      label: 'upload from image gallary',
      command: 'AddImageGallary',
      className: "fa-picture-o imgGlry"
    });
    CKEDITOR.instances.blogEditor.ui.addButton('SaveRecipe', {
      label: 'Save Recipe',
      command: 'SaveRecipe',
      className: "fa-save SaveRecipe"
    });
    CKEDITOR.instances.blogEditor.ui.addButton('Cancel', {
      label: 'Cancel',
      command: 'cancel-cecipe',
      className: "recipe-cancel"
    });
    setTimeout(() => {
      $('.imgGlry span').eq(0).remove();
      $('.SaveRecipe span').eq(0).remove();
      $('.recipe-cancel span').eq(0).remove();
      $('.recipe-cancel span').eq(0).css('display', 'block')
    }, 1000);
    CKEDITOR.instances.blogEditor.addContentsCss('assets/css/css-bootstrap.min.css');
    CKEDITOR.instances.blogEditor.addContentsCss('assets/css/css-animations.css');
    CKEDITOR.instances.blogEditor.addContentsCss('assets/css/css-main.css');
    CKEDITOR.instances.blogEditor.addContentsCss('assets/css/styles.css');
    CKEDITOR.instances.blogEditor.addContentsCss('assets/css/css-font-awesome.css');
    CKEDITOR.instances.blogEditor.addCommand('AddCustomImage', {
      exec: common.uploadImage
    });
    CKEDITOR.instances.blogEditor.addCommand('AddImageGallary', {
      exec: function () {
        $('#MediaGallaryPopup').modal({ backdrop: 'static' });
        $('#txtSearch').val('');
        eval('window.createBlog.PageCount=0;window.createBlog.PageNo=0;window.createBlog.Search="";');
        eval('window.createBlog.bindGallary();');
      }
    });
    CKEDITOR.instances.blogEditor.addCommand('SaveRecipe', {
      exec: function () {
        $('#SaveRecipePopup').modal({ backdrop: 'static' });

      }
    });
    CKEDITOR.instances.blogEditor.addCommand('cancel-cecipe', {
      exec: function () {
        location.href = location.origin + "/#/Recipes";
      }
    });
    if (localStorage.BlogID == undefined) {
      CKEDITOR.instances.blogEditor.setData(common.RecipeTemplate(), {
        callback: function () {
          this.checkDirty(); // true
        }
      });
    }
    else {
      eval('window.createBlog.GetRecipe(' + localStorage.BlogID + ');');

    }
  }
  uploadGallary() {
    var fileInput = $('<input type="file"/>');
    fileInput.change(function () {
      var files = this.files;
      if (files && files[0]) {
        common.UploadFile(this.files[0], function (data) {
          var Media: any = {};
          Media.FilePath = data;
          Media.FileName = files[0].name;
          eval('window.createBlog.lstMedia.unshift(Media)');
          eval('window.createBlog.aa.detectChanges();');
          // Editor.insertHtml('<img src="' + common.ApiURL + "/File/Download?path=" + data + '">');
          //   $('.media-img').prepend(`
          //  <div class="col-2">
          //  <div title="${files[0].name}" style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: 100%;">${files[0].name}</div>
          //  <img onclick="Common.SelectImg(this)" title="${files[0].name}" style="width:100%;"
          //      src="${common.ApiURL}/File/Download?path=${data}">
          // </div>
          //  `);
        });
      }
    });
    fileInput.click();
  }
  pageClick(event) {
    this.PageNo = parseInt(event.currentTarget.dataset.index);
    this.bindGallary();
  }
  SearchClick() {
    this.Search = $('#txtSearch').val();
    this.PageNo = 0;
    this.PageCount = 0;
    this.bindGallary();
  }
  ClearSearchClick() {
    $('#txtSearch').val('');
    this.PageNo = 0;
    this.PageCount = 0;
    this.Search = "";
    this.bindGallary();
  }
  ClearTextClick() {
    $('#txtSearch').val('');
  }
  SaveClick(event) {
    var Recipe = $('#txtRecipe').val();
    if (Recipe.trim() == '') {
      $('.recipe-error').text('Recipe name required.')
    }
    else {
      $('.recipe-error').empty();
    }
    $(event.currentTarget).attr('disabled', true);
    $(event.currentTarget).html('<i class="fa fa-refresh fa-spin"></i>');
    var param: any = {};
    //param.recipe = {};
    param.Name = Recipe;
    param.Contents = CKEDITOR.instances.blogEditor.getData();
    param.ImgURL = $(param.Contents).find('img').eq(0).attr('src');
    if (!isNaN(parseInt($('#txtRecipe').data('id'))) && parseInt($('#txtRecipe').data('id')) > 0) {
      param.ID = $('#txtRecipe').data('id');
    }
    //$('#txtRecipe').data('id');
    $.ajax({
      type: "POST",
      url: common.ApiURL + "File/Recipes",
      data: param,
      success: function (data) {
        vModal.Success("Saved successfully", function () {
          location.href = location.origin + "/#/Recipes";
          location.reload();
        })

      },
      error: function (e) {
        console.log(e);
        $(event.target).attr('disabled', true);
        $(event.target).html('<i class="fa fa-refresh fa-spin"></i>');
      }
    });
  }
  bindGallary() {
    eval('window.createBlog.lstMedia=null');
    eval('window.createBlog.aa.detectChanges();');
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/GetMedia?PageOffset=" + (this.PageNo * this.PageSize) + "&PageSize=" + this.PageSize + "&Search=" + this.Search,
      data: data,
      success: function (data) {
        eval('window.createBlog.lstMedia=data');
        if (data.length > 0) {
          eval('window.createBlog.PageCount=Math.ceil(data[0].TotalCount/window.createBlog.PageSize)');
        }
        else {
          eval('window.createBlog.PageCount=0');
        }
        eval('window.createBlog.aa.detectChanges();');
      },
      error: function (e) {
        console.log(e);
      }
    });
  }


  GetRecipe(ID) {
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/Recipes?ID=" + ID,
      data: data,
      success: function (data) {
        CKEDITOR.instances.blogEditor.setData(data.Contents, {
          callback: function () {
            this.checkDirty(); // true
          }
        });
        $('#txtRecipe').val(data.Name);
        $('#txtRecipe').data('id', data.ID);
      },
      error: function (e) {
        console.log(e);
      }
    });
  }
}
