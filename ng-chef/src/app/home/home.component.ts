import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { common } from '../common-model';
declare var documentReadyInit: any;
declare var windowLoadInit: any;
declare var $: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  PageSize: number = 3;
  PageNo: number = 0;
  PageCount: number = 0;
  Search: string = "";
  lstRecipe: any = [];
  constructor(public aa: ChangeDetectorRef) { }

  ngOnInit() {
    eval('window.self1=this');
    var el = document.getElementsByClassName('page_header_wrapper')[0] as HTMLElement;
    el.classList.remove('clsheader');
    windowLoadInit();
    setTimeout(() => {
      $('.open-subscribe').click(function () {
        common.OpenSubscribe(true);
      });
    }, 1000);
    this.bindRecipe();
  }
  loadScript(Url) {
    let body = <HTMLDivElement>document.body;
    let script = document.createElement('script');
    script.innerHTML = '';
    script.src = Url;
    script.async = true;
    script.defer = true;
    body.appendChild(script);
  }
  bindRecipe() {
    eval('window.self1.lstRecipe=null');
    eval('window.self1.aa.detectChanges();');
    var data = {};
    $.ajax({
      type: "GET",
      url: common.ApiURL + "File/Recipes?PageOffset=" + (this.PageNo * this.PageSize) + "&PageSize=" + this.PageSize + "&Search=" + this.Search,
      data: data,
      success: function (data) {
        eval('window.self1.lstRecipe=data');
        eval('window.self1.aa.detectChanges();');
      },
      error: function (e) {
        console.log(e);
      }
    });
  }
}
