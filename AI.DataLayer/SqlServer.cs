﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
namespace AI.DataLayer
{
    public static class SqlServer
    {
        public static string ConnectionString { get; set; }

        /// <summary>
        /// Insert or update data with json string to db table
        /// </summary>
        /// <param name="ConStr"></param>
        /// <param name="tblName"></param>
        /// <param name="Params"></param>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static bool AddOrUpdate(this string ConStr, string tblName, string Params, out string Message)
        {
            string CmdType = "";
            Message = "";
            bool res = false;
            if (ConStr.ChkConStr())
            {
                Message = "ConnectionString is not valid..";
                throw new Exception(Message);
            }
            if (ConStr.ExecNonQry(ConStr.GetAddOrUpdateCommond(tblName, Params, out CmdType)) > 0)
            {
                Message = "R ecord " + CmdType + " Successfully";
                res = true;
            }
            else
                Message = "Somthing went wrong...";
            return res;
        }
        public static bool AddOrUpdate(this string ConStr, string tblName, out string Message, params object[] Params)
        {
            string CmdType = "";
            Message = "";
            bool res = false;
            try
            {
                if (ConStr.ChkConStr())
                {
                    Message = "ConnectionString is not valid..";
                    throw new Exception(Message);
                }
                if (ConStr.ExecNonQry(ConStr.GetAddOrUpdateCommond(tblName, Params, out CmdType)) > 0)
                    Message = "Record " + CmdType + " Successfully";
                res = true;
            }
            catch (Exception ex)
            {
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return res;
        }
        public static bool AddOrUpdate<T>(this string ConStr, string tblName, List<T> lstBranchNetwork, out string Message)
        {
            Message = "Saved.";
            bool res = false;
            var Qry = "select* from " + tblName;
            try
            {
                var table = ConStr.GetTableSchema(tblName);
                table = table.Fill(lstBranchNetwork);
                using (SqlConnection con = new SqlConnection(ConStr))
                {
                    var da = new SqlDataAdapter(Qry, con);
                    var builder = new SqlCommandBuilder(da);
                    var dt = new DataTable();
                    da.Fill(dt);
                    dt.UpdateDataTable(table, new List<string>());
                    con.Open();
                    da.Update(dt);
                    con.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                ConStr.ErrorLog(ex.Message + ex.StackTrace);
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return res;
        }
        public static bool ChkRecords(this string ConStr, string tblName, string Condition)
        {
            bool res = false;
            try
            {
                if (int.Parse(ConStr.ExecuteScalar("select count(*) from " + tblName + Condition).ToString()) > 0)
                    res = true;
            }
            catch (Exception)
            {

            }
            return res;
        }
        public static int GetIdentity(this string ConStr, string tblName)
        {
            int res = 0;
            try
            {
                res = int.Parse(ConStr.ExecuteScalar("select IDENT_CURRENT('" + tblName + "')").ToString());
            }
            catch (Exception)
            {

            }
            return res;
        }
        public static List<string> GetPK(this string ConStr, string tblName)
        {
            List<string> res = new List<string>();
            var dt = ConStr.ExecQry("exec sp_pkeys " + tblName).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                res.Add(row["Column_name"].ToString());
            }
            return res;
        }
        public static DataTable GetTableInfo(this string ConStr, string tblName)
        {
            return ConStr.ExecQry("sp_columns " + tblName).Tables[0];
        }

        private static SqlCommand GetAddOrUpdateCommond(this string ConStr, string tblName, object[] Params, out string CmdType)
        {
            DataTable tblInfo = ConStr.GetTableInfo(tblName);
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            int i = 0;
            foreach (DataRow row in tblInfo.Rows)
            {
                Fields.Add(row["Column_Name"].ToString().Trim());
                FieldsType.Add(row["Type_Name"].ToString().Trim());
                ValuesParams.Add("@" + row["Column_Name"].ToString().Trim());
                if (Params.Length <= i)
                    continue;
                //if (row["Column_Name"].ToString().Trim().Contains("identity"))
                //    Values.Add(ConStr.GetIdentity(tblName));
                //else
                Values.Add(Params[i++]);
            }
            return ConStr.GetCmd(tblName, Fields, FieldsType, ValuesParams, Values, out CmdType);
        }
        private static SqlCommand GetAddOrUpdateCommond(this string ConStr, string tblName, string Params, out string CmdType)
        {
            JObject jsonData = JObject.Parse(HttpUtility.UrlDecode(Params));
            JToken jtoken = jsonData.First;
            DataTable tblInfo = ConStr.GetTableInfo(tblName);
            string FieldName;
            object FieldValue;
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            for (int i = 0; i < jsonData.Count; i++)
            {
                if (i == 0)
                    jtoken = jsonData.First;
                else
                    jtoken = jtoken.Next;
                FieldName = ((JProperty)jtoken).Name.ToString().ToUpper();
                FieldValue = ((JProperty)jtoken).Value.ToString();
                foreach (DataRow row in tblInfo.Rows)
                {
                    if (row["Column_Name"].ToString().Trim().ToUpper() == FieldName)
                    {
                        Fields.Add(FieldName);
                        ValuesParams.Add("@" + FieldName);
                        FieldsType.Add(row["Type_Name"].ToString().Trim());
                        if (row["Type_Name"].ToString().Trim() == "image")
                            FieldValue = FieldValue.ToString().GetByteArreyfromBase64Img();
                        Values.Add(FieldValue);
                        break;
                    }
                }
            }
            return ConStr.GetCmd(tblName, Fields, FieldsType, ValuesParams, Values, out CmdType);
        }
        private static SqlCommand GetCmd(this string ConStr, string tblName, List<string> Fields, List<string> FieldsType, List<string> ValuesParams, List<object> Values, out string CmdType)
        {
            var Pk = ConStr.GetPK(tblName);
            string Condition = GetCondition(Fields, Values, Pk);
            foreach (var item in Pk)
            {
                if (Fields.Contains(item.ToUpper()))
                {
                    var index = Fields.IndexOf(item.ToUpper());
                    Fields.RemoveAt(index);
                    Values.RemoveAt(index);
                }
            }
            string Qry = "";
            if (ConStr.ChkRecords(tblName, Condition))
            {
                Qry = "Update " + tblName + " set ";
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (Pk.IndexOf(Fields[ii]) >= 0)
                        continue;
                    if (Qry == "Update " + tblName + " set ")
                    {
                        Qry = Qry + Fields[ii] + " = " + ValuesParams[ii];
                    }
                    else
                    {
                        Qry = Qry + "," + Fields[ii] + " = " + ValuesParams[ii];
                    }
                }

                Qry = Qry + Condition;
                CmdType = "Updated";
            }
            else
            {
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (FieldsType[ii].Trim().Contains("identity"))
                    {
                        Fields.RemoveAt(ii);
                        ValuesParams.RemoveAt(ii);
                        Values.RemoveAt(ii);
                        FieldsType.RemoveAt(ii);
                    }
                }
                Qry = "insert into " + tblName + " (" + string.Join(",", Fields) + ") values (" + string.Join(",", ValuesParams) + ")";
                CmdType = "Inserted";
            }
            SqlCommand cmd = new SqlCommand(Qry, new SqlConnection(ConStr));
            for (int ii = 0; ii < ValuesParams.Count; ii++)
            {
                if (Values.Count > ii)
                    cmd.Parameters.AddWithValue(ValuesParams[ii], Values[ii]);
                else
                {
                    if (FieldsType[ii] == "image")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], new byte[10]);
                    else if (FieldsType[ii] == "datetime")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DateTime.Now);
                    else
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DBNull.Value);
                }
            }
            return cmd;
        }
        private static string GetCondition(List<string> Fields, List<object> Values, List<string> Pk)
        {
            string Condition = " Where ";
            for (int ii = 0; ii < Pk.Count; ii++)
            {
                object value = "";
                try
                {
                    value = Values[Fields.IndexOf(Pk[ii].ToUpper())];
                }
                catch (Exception)
                {

                }
                if (ii == 0)
                    Condition = Condition + Pk[ii] + " = '" + value + "'";
                else
                    Condition = " and " + Condition + Pk[ii] + " = '" + value + "'";
            }
            return Condition;
        }
        private static bool ChkConStr(this string ConStr)
        {
            bool res = true;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                    res = false;
            }
            return res;
        }

        public static DataSet ExecQry(this string ConStr, string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public static DataSet ExecQry(this string ConStr, SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                cmd.Connection = con;
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public static DataSet ExecProc(this string ConStr, string ProcName, params object[] Params)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public static int ExecNonQry(this string ConStr, string Qry)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                con.Open();
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public static int ExecNonQry(this string ConStr, SqlCommand cmd)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                cmd.Connection = con;
                con.Open();
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public static int ExecNonQry(this string ConStr, string ProcName, params object[] Params)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public static object ExecuteScalar(this string ConStr, SqlCommand cmd)
        {
            object res = null;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                cmd.Connection = con;
                con.Open();
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public static object ExecuteScalar(this string ConStr, string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            object res = null;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                con.Open();
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public static object ExecuteScalarFromProc(this string ConStr, string ProcName, params object[] Params)
        {
            object res = null;
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public static DataTable GetTableSchema(this string ConStr, string TableName)
        {
            DataTable Table = new DataTable();
            using (SqlConnection con = new SqlConnection(ConStr))
            {
                SqlCommand cmd = new SqlCommand("select * from " + TableName, con);
                var table = (new SqlDataAdapter(cmd)).FillSchema(Table, SchemaType.Source);
            }
            return Table;
        }
        public static void ErrorLog(this string ConStr, string Error)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConStr))
                {
                    string Qry = "";
                    SqlCommand cmd = new SqlCommand("select * from ErrorLog", con);
                    DataTable dt = new DataTable();
                    (new SqlDataAdapter(cmd)).Fill(dt);
                    if (dt.Rows.Count > 1000)
                    {
                        Qry = "delete from ErrorLog; ";
                    }
                    Qry = Qry + "insert into ErrorLog (Error,ErrorTime) values('" + Error.Replace("'", "''") + "', getdate() )";
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }
    }
    public class SqlServerDB
    {
        public string ConnectionString { get; set; }
        public bool AddOrUpdate(string tblName, string Params, out string Message)
        {
            string CmdType = "";
            Message = "";
            bool res = false;
            if (ChkConStr())
            {
                Message = "ConnectionString is not valid..";
                throw new Exception(Message);
            }
            if (ExecNonQry(GetAddOrUpdateCommond(tblName, Params, out CmdType)) > 0)
            {
                Message = "Record " + CmdType + " Successfully";
                res = true;
            }
            else
                Message = "Somthing went wrong...";
            return res;
        }
        public bool AddOrUpdate(string tblName, out string Message, params object[] Params)
        {
            string CmdType = "";
            Message = "";
            bool res = false;
            try
            {
                if (ChkConStr())
                {
                    Message = "ConnectionString is not valid..";
                    throw new Exception(Message);
                }
                if (ExecNonQry(GetAddOrUpdateCommond(tblName, Params, out CmdType)) > 0)
                    Message = "Record " + CmdType + " Successfully";
                res = true;
            }
            catch (Exception ex)
            {
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return res;
        }
        public bool AddOrUpdate<T>(string tblName, List<T> lstData, out string Message)
        {
            Message = "Saved.";
            bool res = false;
            var Qry = "select* from " + tblName;
            try
            {
                var table = GetTableSchema(tblName);
                table = table.Fill(lstData);
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    var da = new SqlDataAdapter(Qry, con);
                    var builder = new SqlCommandBuilder(da);
                    var dt = new DataTable();
                    da.Fill(dt);
                    dt.UpdateDataTable(table, new List<string>());
                    con.Open();
                    da.Update(dt);
                    con.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex.Message + ex.StackTrace);
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return res;
        }
        public bool BulkOperation(string TableName, DataTable dt)
        {
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                string tmpTable = "SELECT * INTO #TempTable from " + TableName + " where 1<>1";
                SqlCommand cmd = new SqlCommand(tmpTable, con);
                var i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                    using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                    {
                        bulk.DestinationTableName = "#TempTable";
                        bulk.WriteToServer(dt);
                    }
                    string mergeSql = "merge into " + TableName + " as Target " +
                                 "using #TempTable as Source " +
                                 "on " +
                                 "Target.Symbol=Source.Symbol " +
                                 "and Target.Timestamp = Source.Timestamp " +
                                 "when matched then " +
                                 "update set Target.Price=Source.Price " +
                                 "when not matched then " +
                                 "insert (Symbol,Price,Timestamp) values (Source.Symbol,Source.Price,Source.Timestamp);";

                    cmd.CommandText = mergeSql;
                    cmd.ExecuteNonQuery();

                    //Clean up the temp table
                    cmd.CommandText = "drop table #Prices";
                    cmd.ExecuteNonQuery();
                }
            }
            return false;
        }
        public bool ChkRecords(string tblName, string Condition)
        {
            bool res = false;
            try
            {
                if (int.Parse(ExecuteScalar("select count(*) from " + tblName + Condition).ToString()) > 0)
                    res = true;
            }
            catch (Exception)
            {

            }
            return res;
        }
        public int GetIdentity(string tblName)
        {
            int res = 0;
            try
            {
                res = int.Parse(ExecuteScalar("select IDENT_CURRENT('" + tblName + "')").ToString());
            }
            catch (Exception)
            {

            }
            return res;
        }
        public List<string> GetPK(string tblName)
        {
            List<string> res = new List<string>();
            var dt = ExecQry("exec sp_pkeys " + tblName).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                res.Add(row["Column_name"].ToString());
            }
            return res;
        }
        public DataTable GetTableInfo(string tblName)
        {
            return ExecQry("sp_columns " + tblName).Tables[0];
        }

        private SqlCommand GetAddOrUpdateCommond(string tblName, object[] Params, out string CmdType)
        {
            DataTable tblInfo = GetTableInfo(tblName);
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            int i = 0;
            foreach (DataRow row in tblInfo.Rows)
            {
                Fields.Add(row["Column_Name"].ToString().Trim());
                FieldsType.Add(row["Type_Name"].ToString().Trim());
                ValuesParams.Add("@" + row["Column_Name"].ToString().Trim());
                if (Params.Length <= i)
                    continue;
                //if (row["Column_Name"].ToString().Trim().Contains("identity"))
                //    Values.Add(GetIdentity(tblName));
                //else
                Values.Add(Params[i++]);
            }
            return GetCmd(tblName, Fields, FieldsType, ValuesParams, Values, out CmdType);
        }
        private SqlCommand GetAddOrUpdateCommond(string tblName, string Params, out string CmdType)
        {
            JObject jsonData = JObject.Parse(HttpUtility.UrlDecode(Params));
            JToken jtoken = jsonData.First;
            DataTable tblInfo = GetTableInfo(tblName);
            string FieldName;
            object FieldValue;
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            for (int i = 0; i < jsonData.Count; i++)
            {
                if (i == 0)
                    jtoken = jsonData.First;
                else
                    jtoken = jtoken.Next;
                FieldName = ((JProperty)jtoken).Name.ToString().ToUpper();
                FieldValue = ((JProperty)jtoken).Value.ToString();
                foreach (DataRow row in tblInfo.Rows)
                {
                    if (row["Column_Name"].ToString().Trim().ToUpper() == FieldName)
                    {
                        Fields.Add(FieldName);
                        ValuesParams.Add("@" + FieldName);
                        FieldsType.Add(row["Type_Name"].ToString().Trim());
                        if (row["Type_Name"].ToString().Trim() == "image")
                            FieldValue = FieldValue.ToString().GetByteArreyfromBase64Img();
                        Values.Add(FieldValue);
                        break;
                    }
                }
            }
            return GetCmd(tblName, Fields, FieldsType, ValuesParams, Values, out CmdType);
        }
        private SqlCommand GetCmd(string tblName, List<string> Fields, List<string> FieldsType, List<string> ValuesParams, List<object> Values, out string CmdType)
        {
            var Pk = GetPK(tblName);
            string Condition = GetCondition(Fields, Values, Pk);
            foreach (var item in Pk)
            {
                if (Fields.Contains(item.ToUpper()))
                {
                    var index = Fields.IndexOf(item.ToUpper());
                    Fields.RemoveAt(index);
                    Values.RemoveAt(index);
                }
            }
            string Qry = "";
            if (ChkRecords(tblName, Condition))
            {
                Qry = "Update " + tblName + " set ";
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (Pk.IndexOf(Fields[ii]) >= 0)
                        continue;
                    if (Qry == "Update " + tblName + " set ")
                    {
                        Qry = Qry + Fields[ii] + " = " + ValuesParams[ii];
                    }
                    else
                    {
                        Qry = Qry + "," + Fields[ii] + " = " + ValuesParams[ii];
                    }
                }

                Qry = Qry + Condition;
                CmdType = "Updated";
            }
            else
            {
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (FieldsType[ii].Trim().Contains("identity"))
                    {
                        Fields.RemoveAt(ii);
                        ValuesParams.RemoveAt(ii);
                        Values.RemoveAt(ii);
                        FieldsType.RemoveAt(ii);
                    }
                }
                Qry = "insert into " + tblName + " (" + string.Join(",", Fields) + ") values (" + string.Join(",", ValuesParams) + ")";
                CmdType = "Inserted";
            }
            SqlCommand cmd = new SqlCommand(Qry, new SqlConnection(ConnectionString));
            for (int ii = 0; ii < ValuesParams.Count; ii++)
            {
                if (Values.Count > ii)
                    cmd.Parameters.AddWithValue(ValuesParams[ii], Values[ii]);
                else
                {
                    if (FieldsType[ii] == "image")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], new byte[10]);
                    else if (FieldsType[ii] == "datetime")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DateTime.Now);
                    else
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DBNull.Value);
                }
            }
            return cmd;
        }
        private string GetCondition(List<string> Fields, List<object> Values, List<string> Pk)
        {
            string Condition = " Where ";
            for (int ii = 0; ii < Pk.Count; ii++)
            {
                object value = "";
                try
                {
                    value = Values[Fields.IndexOf(Pk[ii].ToUpper())];
                }
                catch (Exception)
                {

                }
                if (ii == 0)
                    Condition = Condition + Pk[ii] + " = '" + value + "'";
                else
                    Condition = " and " + Condition + Pk[ii] + " = '" + value + "'";
            }
            return Condition;
        }
        private bool ChkConStr()
        {
            bool res = true;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                    res = false;
            }
            return res;
        }

        public DataSet ExecQry(string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public DataSet ExecQry(SqlCommand cmd)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                cmd.Connection = con;
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public DataSet ExecProc(string ProcName, params object[] Params)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                (new SqlDataAdapter(cmd)).Fill(ds);
            }
            return ds;
        }
        public int ExecNonQry(string Qry)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                con.Open();
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public int ExecNonQry(SqlCommand cmd)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                cmd.Connection = con;
                con.Open();
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public int ExecNonQry(string ProcName, params object[] Params)
        {
            int res = -1;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                res = cmd.ExecuteNonQuery();
            }
            return res;
        }
        public object ExecuteScalar(SqlCommand cmd)
        {
            object res = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                cmd.Connection = con;
                con.Open();
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public object ExecuteScalar(string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            object res = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(Qry, con);
                con.Open();
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public object ExecuteScalarFromProc(string ProcName, params object[] Params)
        {
            object res = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand(ProcName, con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlCommandBuilder.DeriveParameters(cmd);
                var i = 1;
                foreach (var item in Params)
                {
                    cmd.Parameters[i++].Value = item;
                }
                res = cmd.ExecuteScalar();
            }
            return res;
        }
        public DataTable GetTableSchema(string TableName)
        {
            DataTable Table = new DataTable();
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("select * from " + TableName, con);
                var table = (new SqlDataAdapter(cmd)).FillSchema(Table, SchemaType.Source);
            }
            return Table;
        }
        public void ErrorLog(string Error)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string Qry = "";
                    SqlCommand cmd = new SqlCommand("select * from ErrorLog", con);
                    DataTable dt = new DataTable();
                    (new SqlDataAdapter(cmd)).Fill(dt);
                    if (dt.Rows.Count > 1000)
                    {
                        Qry = "delete from ErrorLog; ";
                    }
                    Qry = Qry + "insert into ErrorLog (Error,ErrorTime) values('" + Error.Replace("'", "''") + "', getdate() )";
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }
    }

}
