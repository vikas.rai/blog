﻿using AI.CommonLayer.Generics;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace AI.DataLayer
{
    public class Database
    {
        public DbProviderFactory Factory { get; set; }
        public string ConnectionString { get; set; }
        public string ParameterPrefix { get; set; }
        private static DatabaseFactory _database { get; set; }

        public Database(ConnectionStringSettings Setting, string _ParameterPrefix = null)
        {
            _ParameterPrefix = !string.IsNullOrEmpty(_ParameterPrefix) ? _ParameterPrefix : (Setting.ProviderName == "System.Data.SqlClient" ? "@" : "P_");
            ConnectionString = Setting.ConnectionString;
            ParameterPrefix = _ParameterPrefix;
            Factory = DbProviderFactories.GetFactory(Setting.ProviderName);
        }

        public static DatabaseFactory GetDatabase()
        {
            if (_database == null)
            {
                _database = new DatabaseFactory();
            }
            return _database;
        }

        public virtual async Task<DbTransaction> GetTransaction()
        {
            var connection = await GetConnection();
            return connection.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        public async Task<DbConnection> GetConnection()
        {
            var connection = Factory.CreateConnection();
            connection.ConnectionString = ConnectionString;
            await connection.OpenAsync();
            return connection;
        }

        public async Task<int> ExecuteNonQuery(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteNonQuery(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<int> ExecuteNonQuery(DbTransaction Transaction, string Query)
        {
            return await ExecuteNonQuery(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<int> ExecuteNonQuery(string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteNonQuery(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<int> ExecuteNonQuery(string Query)
        {
            return await ExecuteNonQuery(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<IDataReader> GetReader(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetReader(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<IDataReader> GetReader(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetReader(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<IDataReader> GetReader(string procedure, Dictionary<string, object> parameters)
        {
            return await GetReader(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<IDataReader> GetReader(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetReader(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataSet> GetDataSet(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataSet(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataSet> GetDataSet(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataSet(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataSet> GetDataSet(string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataSet(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataSet> GetDataSet(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataSet(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataTable> GetDataTable(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataTable(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataTable> GetDataTable(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataTable(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataTable> GetDataTable(string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataTable(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataTable> GetDataTable(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataTable(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public void CloseConnection(DbConnection connection)
        {
            connection.Close();
            connection.Dispose();
            connection = null;
        }
        public void CloseConnection(DbTransaction Transaction)
        {
            Transaction.Dispose();
            Transaction.Connection.Close();
            Transaction.Connection.Dispose();
        }
        public async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await this.GetObjectList<T>(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await this.GetObjectList<T>(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<List<T>> GetObjectList<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await this.GetObjectList<T>(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<List<T>> GetObjectList<T>(string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await this.GetObjectList<T>(Query, CommandType.StoredProcedure, new Dictionary<string, object>());
        }
        public async Task<T> GetObject<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await GetObject<T>(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<T> GetObject<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetObject<T>(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<T> GetObject<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await GetObject<T>(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<T> GetObject<T>(string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetObject<T>(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<object> ExecuteScalar(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteScalar(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<object> ExecuteScalar(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await ExecuteScalar(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<object> ExecuteScalar(string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteScalar(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<object> ExecuteScalar(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await ExecuteScalar(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<Tuple<bool, string>> BulkInsert(DbTransaction Transaction, string CommandText, DataTable dt)
        {
            bool IsSuccess = false;
            string error = null;
            await Task.Run(() =>
            {
                try
                {
                    using (DbCommand cmd = Factory.CreateCommand())
                    {
                        cmd.Connection = Transaction.Connection;
                        cmd.Transaction = Transaction;
                        cmd.CommandText = CommandText;

                        using (DbDataAdapter da = Factory.CreateDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.UpdateBatchSize = dt.Rows.Count;
                            using (DbCommandBuilder cb = Factory.CreateCommandBuilder())
                            {
                                cb.DataAdapter = da;
                                int i = da.Update(dt);
                            }
                            IsSuccess = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    IsSuccess = false;
                    error = ex.Message;
                }
            });
            return new Tuple<bool, string>(IsSuccess, error);
        }
        public async Task<Tuple<bool, string>> BulkAddOrUpdate<T>(string tblName, List<T> lstData, string Condition)
        {
            string Message = "Saved.";
            bool res = false;
            try
            {
                var Qry = "select* from " + tblName + " " + Condition;
                var table = await GetTableSchema(tblName);
                table = table.Fill(lstData);
                using (DbConnection con = await GetConnection())
                {
                    var da = Factory.CreateDataAdapter();
                    da.SelectCommand = Factory.CreateCommand();
                    da.SelectCommand.CommandText = Qry;
                    da.SelectCommand.Connection = con;

                    var builder = Factory.CreateCommandBuilder();
                    builder.DataAdapter = da;
                    var dt = new DataTable();
                    da.Fill(dt);
                    con.Open();
                    da.Update(table);
                    con.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return Tuple.Create(res, Message);
        }
        public async Task<List<string>> GetPK(string tblName)
        {
            List<string> res = new List<string>();
            var dt = (await GetDataSet("exec sp_pkeys " + tblName)).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                res.Add(row["Column_name"].ToString());
            }
            return res;
        }
        public async Task<bool> ChkRecords(string tblName, string Condition)
        {
            return Convert.ToInt32(await ExecuteScalar("select count(*) from " + tblName + Condition)) > 0;
        }
        public async Task<DataTable> GetTableSchema(string TableName)
        {
            DataTable Table = new DataTable();
            var dt = await GetDataSet("select * from " + TableName + "WHERE 1=0");
            if (dt.Tables.Count > 0)
            {
                return dt.Tables[0];
            }
            return Table;
        }
        public DbType GetDBType(Type theType)
        {
            DbParameter p1;
            TypeConverter tc;
            p1 = Factory.CreateParameter();
            tc = TypeDescriptor.GetConverter(p1.DbType);
            if (tc.CanConvertFrom(theType))
            {
                p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
            }
            else
            {
                //Try brute force
                try
                {
                    p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
                }
                catch (Exception)
                {
                    //Do Nothing; will return NVarChar as default
                }
            }
            return p1.DbType;
        }
        public DbCommand GetCommand(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var cmd = Factory.CreateCommand();
            cmd.CommandText = CommandText;
            cmd.Transaction = Transaction;
            cmd.Connection = Transaction.Connection;
            cmd.CommandType = commandType;
            cmd.CommandTimeout = 3600;

            cmd.Parameters.AddRange(GetDbParameter(parameters).ToArray());

            if (commandType == CommandType.Text)
            {
                cmd.Prepare();
            }

            return cmd;
        }
        public DbCommand GetCommand(DbConnection Connection, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var cmd = Factory.CreateCommand();
            cmd.Connection = Connection;
            cmd.CommandText = CommandText;
            cmd.CommandType = commandType;
            cmd.CommandTimeout = 3600;
            cmd.Parameters.AddRange(GetDbParameter(parameters).ToArray());
            if (commandType == CommandType.Text)
            {
                cmd.Prepare();
            }
            return cmd;
        }
        public List<DbParameter> GetDbParameter(Dictionary<string, object> parameters)
        {
            List<DbParameter> Params = new List<DbParameter>();
            if (parameters != null)
            {
                foreach (var parameter in parameters.Keys)
                {
                    var name = parameter;
                    var p = Factory.CreateParameter();

                    if (parameter.Contains("o:"))
                    {
                        p.Direction = ParameterDirection.Output;
                        name = parameter.Substring(parameter.IndexOf(':') + 1);
                    }

                    p.ParameterName = ParameterPrefix + name;
                    //p.ParameterName = name;
                    p.Value = parameters[parameter];

                    Params.Add(p);
                }
            }
            return Params;
        }
        private async Task<int> ExecuteNonQuery(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var result = 0;
            using (DbConnection Connection = await GetConnection())
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                result = await command.ExecuteNonQueryAsync();
                FillOutputParams(command, parameters);
            }
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            var result = await command.ExecuteNonQueryAsync();
            FillOutputParams(command, parameters);
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbCommand cmd)
        {
            var result = 0;
            using (DbConnection Connection = await GetConnection())
            {
                result = await cmd.ExecuteNonQueryAsync();
            }
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbTransaction Transaction, DbCommand cmd)
        {
            var result = await cmd.ExecuteNonQueryAsync();
            return result;
        }
        private async Task<DbDataReader> GetReader(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            var reader = await command.ExecuteReaderAsync();
            FillOutputParams(command, parameters);
            return reader;
        }
        private async Task<IDataReader> GetReader(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            IDataReader reader = null;
            using (DbConnection Connection = await GetConnection())
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                reader = await command.ExecuteReaderAsync();
                FillOutputParams(command, parameters);
            }
            return reader;
        }
        private async Task<DataSet> GetDataSet(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            await Task.Run(() =>
            {
                var command = GetCommand(Transaction, CommandText, commandType, parameters);
                DbDataAdapter da = Factory.CreateDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds);
                FillOutputParams(command, parameters);
            });
            return ds;
        }
        private async Task<DataSet> GetDataSet(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            using (DbConnection Connection = await GetConnection())
            {

                var command = GetCommand(Connection, CommandText, commandType, parameters);
                var da = Factory.CreateDataAdapter();
                da.SelectCommand = command;
                da.Fill(ds);
                FillOutputParams(command, parameters);
            }
            return ds;
        }
        private async Task<DataTable> GetDataTable(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTable dt = new DataTable();
            await Task.Run(() =>
            {
                var command = GetCommand(Transaction, CommandText, commandType, parameters);
                var da = Factory.CreateDataAdapter();
                da.SelectCommand = command;
                da.Fill(dt);
                FillOutputParams(command, parameters);
            });
            return dt;
        }
        private async Task<DataTable> GetDataTable(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTable dt = new DataTable();
            using (DbConnection Connection = await GetConnection())
            {

                var command = GetCommand(Connection, CommandText, commandType, parameters);
                var da = Factory.CreateDataAdapter();
                da.SelectCommand = command;
                da.Fill(dt);
                FillOutputParams(command, parameters);
            }
            return dt;
        }
        private async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            List<T> list = new List<T>();
            var dt = await GetDataTable(Transaction, CommandText, commandType, parameters);
            if (dt.Rows.Count > 0)
            {
                list = dt.ToList<T>();
            }
            return list;
        }
        private async Task<List<T>> GetObjectList<T>(string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            List<T> list = new List<T>();
            var dt = await GetDataTable(CommandText, commandType, parameters);
            if (dt.Rows.Count > 0)
            {
                list = dt.ToList<T>();
            }
            return list;
        }
        private async Task<T> GetObject<T>(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            T Obj = new T();
            var dt = await GetDataTable(Transaction, CommandText, commandType, parameters);
            if (dt.Rows.Count > 0)
            {
                Obj = dt.ToObject<T>();
            }
            return Obj;
        }
        private async Task<T> GetObject<T>(string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            T Obj = new T();
            var dt= await GetDataTable(CommandText, commandType, parameters);
            if (dt.Rows.Count > 0)
            {
                Obj = dt.ToObject<T>();
            }
            return Obj;
        }
        private void FillOutputParams(DbCommand command, Dictionary<string, object> parameters)
        {
            foreach (DbParameter item in command.Parameters)
            {
                if (item.Direction == ParameterDirection.Output)
                {
                    parameters[$"o:{item.ParameterName}"] = item.Value;
                }
            }
        }
        private string GetCondition(List<string> Fields, List<object> Values, List<string> Pk)
        {
            string Condition = " Where ";
            for (int ii = 0; ii < Pk.Count; ii++)
            {
                object value = "";
                try
                {
                    value = Values[Fields.IndexOf(Pk[ii].ToUpper())];
                }
                catch (Exception)
                {

                }
                if (ii == 0)
                    Condition = Condition + Pk[ii] + " = '" + value + "'";
                else
                    Condition = " and " + Condition + Pk[ii] + " = '" + value + "'";
            }
            return Condition;
        }
        private async Task<object> ExecuteScalar(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            object res = await command.ExecuteScalarAsync();
            FillOutputParams(command, parameters);
            return res;
        }
        private async Task<object> ExecuteScalar(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            object res = null;
            using (DbConnection Connection = await GetConnection())
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                res = await command.ExecuteScalarAsync();
                FillOutputParams(command, parameters);
            }
            return res;
        }
        private DataTable UpdateDataTable(DataTable table, DataTable Source, List<string> PK)
        {
            DataRow dr;
            if (PK != null)
            {
                return table;
            }
            foreach (DataRow item in Source.Rows)
            {
                List<DataRow> a = null;
                #region PK Condition
                if (PK.Count == 1)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]))
                    .ToList();
                }
                else if (PK.Count == 2)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]))
                    .ToList();
                }
                else if (PK.Count == 3)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]))
                    .ToList();
                }
                else if (PK.Count == 4)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]))
                    .ToList();
                }
                else if (PK.Count == 5)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]) &&
                    x.Field<int>(PK[4]) == item.Field<int>(PK[4]))
                    .ToList();
                }
                else if (PK.Count == 6)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]) &&
                    x.Field<int>(PK[4]) == item.Field<int>(PK[4]) &&
                    x.Field<int>(PK[5]) == item.Field<int>(PK[5]))
                    .ToList();
                }
                //a = table.AsEnumerable().Where(x =>
                //x.Field<int>("BankID") == item.Field<int>("BankID") &&
                //x.Field<int>("CityID") == item.Field<int>("CityID") &&
                //x.Field<int>("FID") == item.Field<int>("FID"))
                //.ToList();
                #endregion

                if (a != null && a.Count > 0)
                {
                    foreach (DataColumn col in item.Table.Columns)
                    {
                        a[0][col.ColumnName] = item[col.ColumnName];
                    }
                }
                else
                {
                    dr = table.NewRow();
                    foreach (DataColumn col in item.Table.Columns)
                    {
                        dr[col.ColumnName] = item[col.ColumnName];
                    }
                    table.Rows.Add(dr);
                }
            }
            return table;
        }
        public void ErrorLog(string Error)
        {
            try
            {
                using (DbConnection con = GetConnection().Result)
                {
                    string Qry = "";
                    DbCommand cmd = Factory.CreateCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "select * from ErrorLog";
                    DataTable dt = new DataTable();
                    var da = Factory.CreateDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(dt);
                    if (dt.Rows.Count > 1000)
                    {
                        Qry = "delete from ErrorLog; ";
                    }
                    Qry = Qry + "insert into ErrorLog (Error,ErrorTime) values('" + Error.Replace("'", "''") + "', getdate() )";
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }
    }
}
