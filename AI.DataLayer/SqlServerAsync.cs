﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web;
namespace AI.DataLayer
{
    public class SqlServerAsync
    {
        public string ConnectionString { get; set; }
        public SqlServerAsync(string conStr)
        {
            ConnectionString = conStr;
        }
        /// <summary>
        /// Insert or update data with json string to db table
        /// </summary>
        /// <param name="tblName">Name of the table in which data to be inserted.</param>
        /// <param name="Params">Columns value with column name and value in json format.</param>
        /// <returns>if operation success returns true otherwise false.</returns>
        public async Task<int> AddOrUpdate(string tblName, string Params)
        {
            if (ChkConStr())
            {
                throw new Exception("ConnectionString is not valid..");
            }
            return await ExecNonQry(await GetAddOrUpdateCommond(tblName, Params));

        }
        public async Task<int> AddOrUpdate(string tblName, params object[] Params)
        {
            if (ChkConStr())
            {
                throw new Exception("ConnectionString is not valid..");
            }
            return await ExecNonQry(await GetAddOrUpdateCommond(tblName, Params));
        }
        public async Task<bool> AddOrUpdate<T>(string tblName, List<T> lstData)
        {
            bool res = false;
            var Qry = "select* from " + tblName;
            var table = await GetTableSchema(tblName);
            table = table.Fill(lstData);
            await Task.Run(() =>
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    var da = new SqlDataAdapter(Qry, con);
                    var builder = new SqlCommandBuilder(da);
                    var dt = new DataTable();
                    da.Fill(dt);
                    dt.UpdateDataTable(table, new List<string>());
                    con.Open();
                    da.Update(dt);
                    con.Close();
                    res = true;
                }
            });

            return res;
        }
        public async Task<bool> BulkOperation(string TableName, DataTable dt)
        {
            var res = false;
            await Task.Run(() =>
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string tmpTable = "SELECT * INTO #TempTable from " + TableName + " where 1<>1";
                    SqlCommand cmd = new SqlCommand(tmpTable, con);
                    var i = cmd.ExecuteNonQuery();
                    if (i > 0)
                    {
                        using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                        {
                            bulk.DestinationTableName = "#TempTable";
                            bulk.WriteToServer(dt);
                        }
                        string mergeSql = "merge into " + TableName + " as Target " +
                                     "using #TempTable as Source " +
                                     "on " +
                                     "Target.Symbol=Source.Symbol " +
                                     "and Target.Timestamp = Source.Timestamp " +
                                     "when matched then " +
                                     "update set Target.Price=Source.Price " +
                                     "when not matched then " +
                                     "insert (Symbol,Price,Timestamp) values (Source.Symbol,Source.Price,Source.Timestamp);";

                        cmd.CommandText = mergeSql;
                        cmd.ExecuteNonQuery();

                        //Clean up the temp table
                        cmd.CommandText = "drop table #Prices";
                        cmd.ExecuteNonQuery();
                        res = true;
                    }
                    else
                    {
                        res = false;
                    }
                }
            });
            return res;
        }
        public async Task<bool> ChkRecords(string tblName, string Condition)
        {
            return Convert.ToInt32(await ExecuteScalar("select count(*) from " + tblName + Condition)) > 0;
        }
        public async Task<int> GetIdentity(string tblName)
        {
            int res = 0;
            try
            {
                res = Convert.ToInt32(await ExecuteScalar("select IDENT_CURRENT('" + tblName + "')"));
            }
            catch (Exception)
            {

            }
            return res;
        }
        public async Task<List<string>> GetPK(string tblName)
        {
            List<string> res = new List<string>();
            var dt = (await ExecQry("exec sp_pkeys " + tblName)).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                res.Add(row["Column_name"].ToString());
            }
            return res;
        }
        public async Task<DataTable> GetTableInfo(string tblName)
        {
            return (await ExecQry("sp_columns " + tblName)).Tables[0];
        }

        private async Task<SqlCommand> GetAddOrUpdateCommond(string tblName, object[] Params)
        {
            DataTable tblInfo = await GetTableInfo(tblName);
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            int i = 0;
            foreach (DataRow row in tblInfo.Rows)
            {
                Fields.Add(row["Column_Name"].ToString().Trim());
                FieldsType.Add(row["Type_Name"].ToString().Trim());
                ValuesParams.Add("@" + row["Column_Name"].ToString().Trim());
                if (Params.Length <= i)
                    continue;
                //if (row["Column_Name"].ToString().Trim().Contains("identity"))
                //    Values.Add(GetIdentity(tblName));
                //else
                Values.Add(Params[i++]);
            }
            return await GetCmd(tblName, Fields, FieldsType, ValuesParams, Values);
        }
        private async Task<SqlCommand> GetAddOrUpdateCommond(string tblName, string Params)
        {
            JObject jsonData = JObject.Parse(HttpUtility.UrlDecode(Params));
            JToken jtoken = jsonData.First;
            DataTable tblInfo = await GetTableInfo(tblName);
            string FieldName;
            object FieldValue;
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            for (int i = 0; i < jsonData.Count; i++)
            {
                if (i == 0)
                    jtoken = jsonData.First;
                else
                    jtoken = jtoken.Next;
                FieldName = ((JProperty)jtoken).Name.ToString().ToUpper();
                FieldValue = ((JProperty)jtoken).Value.ToString();
                foreach (DataRow row in tblInfo.Rows)
                {
                    if (row["Column_Name"].ToString().Trim().ToUpper() == FieldName)
                    {
                        Fields.Add(FieldName);
                        ValuesParams.Add("@" + FieldName);
                        FieldsType.Add(row["Type_Name"].ToString().Trim());
                        if (row["Type_Name"].ToString().Trim() == "image")
                            FieldValue = FieldValue.ToString().GetByteArreyfromBase64Img();
                        Values.Add(FieldValue);
                        break;
                    }
                }
            }
            return await GetCmd(tblName, Fields, FieldsType, ValuesParams, Values);
        }
        private async Task<SqlCommand> GetCmd(string tblName, List<string> Fields, List<string> FieldsType, List<string> ValuesParams, List<object> Values)
        {
            var Pk =await GetPK(tblName);
            string Condition = GetCondition(Fields, Values, Pk);
            foreach (var item in Pk)
            {
                if (Fields.Contains(item.ToUpper()))
                {
                    var index = Fields.IndexOf(item.ToUpper());
                    Fields.RemoveAt(index);
                    Values.RemoveAt(index);
                }
            }
            string Qry = "";
            if (await ChkRecords(tblName, Condition))
            {
                Qry = "Update " + tblName + " set ";
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (Pk.IndexOf(Fields[ii]) >= 0)
                        continue;
                    if (Qry == "Update " + tblName + " set ")
                    {
                        Qry = Qry + Fields[ii] + " = " + ValuesParams[ii];
                    }
                    else
                    {
                        Qry = Qry + "," + Fields[ii] + " = " + ValuesParams[ii];
                    }
                }

                Qry = Qry + Condition;
            }
            else
            {
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (FieldsType[ii].Trim().Contains("identity"))
                    {
                        Fields.RemoveAt(ii);
                        ValuesParams.RemoveAt(ii);
                        Values.RemoveAt(ii);
                        FieldsType.RemoveAt(ii);
                    }
                }
                Qry = "insert into " + tblName + " (" + string.Join(",", Fields) + ") values (" + string.Join(",", ValuesParams) + ")";
            }
            SqlCommand cmd = new SqlCommand(Qry, new SqlConnection(ConnectionString));
            for (int ii = 0; ii < ValuesParams.Count; ii++)
            {
                if (Values.Count > ii)
                    cmd.Parameters.AddWithValue(ValuesParams[ii], Values[ii]);
                else
                {
                    if (FieldsType[ii] == "image")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], new byte[10]);
                    else if (FieldsType[ii] == "datetime")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DateTime.Now);
                    else
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DBNull.Value);
                }
            }
            return cmd;
        }
        private string GetCondition(List<string> Fields, List<object> Values, List<string> Pk)
        {
            string Condition = " Where ";
            for (int ii = 0; ii < Pk.Count; ii++)
            {
                object value = "";
                try
                {
                    value = Values[Fields.IndexOf(Pk[ii].ToUpper())];
                }
                catch (Exception)
                {

                }
                if (ii == 0)
                    Condition = Condition + Pk[ii] + " = '" + value + "'";
                else
                    Condition = " and " + Condition + Pk[ii] + " = '" + value + "'";
            }
            return Condition;
        }
        private bool ChkConStr()
        {
            bool res = true;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                con.Open();
                if (con.State == ConnectionState.Open)
                    res = false;
            }
            return res;
        }

        public async Task<DataSet> ExecQry(string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");

            return await Task.Run(() =>
            {
                DataSet ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(Qry, con);
                    (new SqlDataAdapter(cmd)).Fill(ds);
                }
                return ds;
            });

        }
        public async Task<DataSet> ExecQry(SqlCommand cmd)
        {
            return await Task.Run(() =>
            {
                DataSet ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    cmd.Connection = con;
                    (new SqlDataAdapter(cmd)).Fill(ds);
                }
                return ds;
            });

        }
        public async Task<DataSet> ExecProc(string ProcName, params object[] Params)
        {
            return await Task.Run(() =>
            {
                DataSet ds = new DataSet();
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(ProcName, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlCommandBuilder.DeriveParameters(cmd);
                    var i = 1;
                    foreach (var item in Params)
                    {
                        cmd.Parameters[i++].Value = item;
                    }
                (new SqlDataAdapter(cmd)).Fill(ds);
                }
                return ds;
            });
        }
        public async Task<int> ExecNonQry(string Qry)
        {
            return await Task.Run(() =>
            {
                int res = -1;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(Qry, con);
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }
                return res;
            });
        }
        public async Task<int> ExecNonQry(SqlCommand cmd)
        {
            return await Task.Run(() =>
            {
                int res = -1;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteNonQuery();
                }
                return res;
            });
        }
        public async Task<int> ExecNonQry(string ProcName, params object[] Params)
        {
            return await Task.Run(() =>
            {
                int res = -1;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(ProcName, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlCommandBuilder.DeriveParameters(cmd);
                    var i = 1;
                    foreach (var item in Params)
                    {
                        cmd.Parameters[i++].Value = item;
                    }
                    res = cmd.ExecuteNonQuery();
                }
                return res;
            });
        }
        public async Task<object> ExecuteScalar(SqlCommand cmd)
        {
            return await Task.Run(() =>
            {
                object res = null;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    cmd.Connection = con;
                    con.Open();
                    res = cmd.ExecuteScalar();
                }
                return res;
            });
        }
        public async Task<object> ExecuteScalar(string Qry)
        {
            if (Qry.IndexOf("delete", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("drop", StringComparison.InvariantCultureIgnoreCase) > 0 || Qry.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await Task.Run(() =>
            {
                object res = null;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(Qry, con);
                    con.Open();
                    res = cmd.ExecuteScalar();
                }
                return res;
            });
        }
        public async Task<object> ExecuteScalarFromProc(string ProcName, params object[] Params)
        {
            return await Task.Run(() =>
            {
                object res = null;
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(ProcName, con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    SqlCommandBuilder.DeriveParameters(cmd);
                    var i = 1;
                    foreach (var item in Params)
                    {
                        cmd.Parameters[i++].Value = item;
                    }
                    res = cmd.ExecuteScalar();
                }
                return res;
            });
        }
        public async Task<DataTable> GetTableSchema(string TableName)
        {
            return await Task.Run(() =>
            {
                DataTable Table = new DataTable();
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("select * from " + TableName, con);
                    var table = (new SqlDataAdapter(cmd)).FillSchema(Table, SchemaType.Source);
                }
                return Table;
            });
        }
        public void ErrorLog(string Error)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string Qry = "";
                    SqlCommand cmd = new SqlCommand("select * from ErrorLog", con);
                    DataTable dt = new DataTable();
                    (new SqlDataAdapter(cmd)).Fill(dt);
                    if (dt.Rows.Count > 1000)
                    {
                        Qry = "delete from ErrorLog; ";
                    }
                    Qry = Qry + "insert into ErrorLog (Error,ErrorTime) values('" + Error.Replace("'", "''") + "', getdate() )";
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }
    }

}
