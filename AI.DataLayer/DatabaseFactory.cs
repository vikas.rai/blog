﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI.DataLayer
{
    public class DatabaseFactory
    {
        private static Database _database { get; set; }

        public static Database GetDatabase()
        {
            if (_database == null)
            {
                var setting = ConfigurationManager.ConnectionStrings["ConStr"];
                _database = new Database(setting);
            }
            return _database;
        }
    }
}
