﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI.DataLayer
{
    public class MySqlDatabase 
    {
       
        public Task<int> AddOrUpdate(string tblName, params object[] Params)
        {
            throw new NotImplementedException();
        }

        public Task<int> AddOrUpdate(string tblName, string Params)
        {
            throw new NotImplementedException();
        }

        public Task<Tuple<bool, string>> BulkAddOrUpdate<T>(string tblName, List<T> lstData, string Condition)
        {
            throw new NotImplementedException();
        }

        public Task<Tuple<bool, string>> BulkInsert(DbTransaction Transaction, string Sql, DataTable dt)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ChkRecords(string tblName, string Condition)
        {
            throw new NotImplementedException();
        }

        public void CloseConnection(DbTransaction Transaction)
        {
            throw new NotImplementedException();
        }

        public void CloseConnection(DbConnection connection)
        {
            throw new NotImplementedException();
        }

        public void ErrorLog(string Error)
        {
            throw new NotImplementedException();
        }

        public Task<int> ExecuteNonQuery(string Query)
        {
            throw new NotImplementedException();
        }

        public Task<int> ExecuteNonQuery(string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<int> ExecuteNonQuery(DbTransaction Transaction, string Query)
        {
            throw new NotImplementedException();
        }

        public Task<int> ExecuteNonQuery(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteScalar(string Query)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteScalar(string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteScalar(DbTransaction Transaction, string Query)
        {
            throw new NotImplementedException();
        }

        public Task<object> ExecuteScalar(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public DbCommand GetCommand(DbConnection Connection, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public DbCommand GetCommand(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }


        public Task<DataSet> GetDataSet(string Query)
        {
            throw new NotImplementedException();
        }

        public Task<DataSet> GetDataSet(string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<DataSet> GetDataSet(DbTransaction Transaction, string Query)
        {
            throw new NotImplementedException();
        }

        public Task<DataSet> GetDataSet(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public List<DbParameter> GetDbParameter(Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public DbType GetDBType(Type theType)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetObject<T>(string Query) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetObject<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetObject<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<T> GetObject<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetObjectList<T>(string Query) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetObjectList<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            throw new NotImplementedException();
        }

        public Task<List<string>> GetPK(string tblName)
        {
            throw new NotImplementedException();
        }

        public Task<IDataReader> GetReader(string Query)
        {
            throw new NotImplementedException();
        }

        public Task<IDataReader> GetReader(string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<IDataReader> GetReader(DbTransaction Transaction, string Query)
        {
            throw new NotImplementedException();
        }

        public Task<IDataReader> GetReader(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetTableInfo(string tblName)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetTableSchema(string TableName)
        {
            throw new NotImplementedException();
        }

        public Task<DbTransaction> GetTransaction()
        {
            throw new NotImplementedException();
        }

        public Task<DbTransaction> GetTransaction(string connectionString)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetDataTable(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetDataTable(DbTransaction Transaction, string Query)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetDataTable(string procedure, Dictionary<string, object> parameters)
        {
            throw new NotImplementedException();
        }

        public Task<DataTable> GetDataTable(string Query)
        {
            throw new NotImplementedException();
        }

       
    }
}
