﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI.DataLayer
{
    public interface IDatabase
    {
        string ConnectionString { get; set; }
        string ParameterPrefix { get; set; }
        Task<DbTransaction> GetTransaction();
        Task<DbTransaction> GetTransaction(string connectionString);
        Task<DbConnection> GetConnection(string connectionString);
        Task<int> ExecuteNonQuery(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters);
        Task<int> ExecuteNonQuery(DbTransaction Transaction, string Query);
        Task<int> ExecuteNonQuery(string procedure, Dictionary<string, object> parameters);
        Task<int> ExecuteNonQuery(string Query);
        Task<IDataReader> GetReader(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters);
        Task<IDataReader> GetReader(DbTransaction Transaction, string Query);
        Task<IDataReader> GetReader(string procedure, Dictionary<string, object> parameters);
        Task<IDataReader> GetReader(string Query);
        Task<DataSet> GetDataSet(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters);
        Task<DataSet> GetDataSet(DbTransaction Transaction, string Query);
        Task<DataSet> GetDataSet(string procedure, Dictionary<string, object> parameters);
        Task<DataSet> GetDataSet(string Query);
        Task<DataTable> GetDataTable(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters);
        Task<DataTable> GetDataTable(DbTransaction Transaction, string Query);
        Task<DataTable> GetDataTable(string procedure, Dictionary<string, object> parameters);
        Task<DataTable> GetDataTable(string Query);

        void CloseConnection(DbConnection connection);
        void CloseConnection(DbTransaction Transaction);
        Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new();
        Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string Query) where T : class, new();
        Task<List<T>> GetObjectList<T>(string procedure, Dictionary<string, object> parameters) where T : class, new();
        Task<List<T>> GetObjectList<T>(string Query) where T : class, new();
        Task<T> GetObject<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new();
        Task<T> GetObject<T>(DbTransaction Transaction, string Query) where T : class, new();
        Task<T> GetObject<T>(string procedure, Dictionary<string, object> parameters) where T : class, new();
        Task<T> GetObject<T>(string Query) where T : class, new();
        Task<object> ExecuteScalar(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters);
        Task<object> ExecuteScalar(DbTransaction Transaction, string Query);
        Task<object> ExecuteScalar(string procedure, Dictionary<string, object> parameters);
        Task<object> ExecuteScalar(string Query);
        Task<Tuple<bool, string>> BulkInsert(DbTransaction Transaction, string Sql, DataTable dt);
        Task<Tuple<bool, string>> BulkAddOrUpdate<T>(string tblName, List<T> lstData, string Condition);
        Task<int> AddOrUpdate(string tblName, string Params);
        Task<int> AddOrUpdate(string tblName, params object[] Params);
        Task<List<string>> GetPK(string tblName);
        Task<DataTable> GetTableInfo(string tblName);
        Task<bool> ChkRecords(string tblName, string Condition);
        Task<DataTable> GetTableSchema(string TableName);
        DbType GetDBType(System.Type theType);
        DbCommand GetCommand(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters);
        DbCommand GetCommand(DbConnection Connection, string CommandText, CommandType commandType, Dictionary<string, object> parameters);
        List<DbParameter> GetDbParameter(Dictionary<string, object> parameters);
        void ErrorLog(string Error);

    }
}
