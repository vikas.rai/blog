﻿using AI.CommonLayer.Generics;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AI.DataLayer
{
    public class SqlDatabase : IDatabase
    {
        public SqlDatabase(string connectionString)
        {
            ConnectionString = connectionString;
            ParameterPrefix = "@";
        }
        public SqlDatabase(string connectionString, string _ParameterPrefix)
        {
            ConnectionString = connectionString;
            ParameterPrefix = _ParameterPrefix;
        }

        public string ConnectionString { get; set; }
        public string ParameterPrefix { get; set; }
        public async Task<DbTransaction> GetTransaction()
        {
            var connection = await GetConnection(ConnectionString);

            return connection.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        public async Task<DbTransaction> GetTransaction(string connectionString)
        {
            var connection = await GetConnection(connectionString);
            return connection.BeginTransaction(IsolationLevel.ReadUncommitted);
        }
        public async Task<DbConnection> GetConnection(string connectionString)
        {
            var connection = new SqlConnection(connectionString);
            await connection.OpenAsync();
            return connection;
        }
        public async Task<int> ExecuteNonQuery(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteNonQuery(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<int> ExecuteNonQuery(DbTransaction Transaction, string Query)
        {
            return await ExecuteNonQuery(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<int> ExecuteNonQuery(string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteNonQuery(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<int> ExecuteNonQuery(string Query)
        {
            return await ExecuteNonQuery(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<IDataReader> GetReader(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetReader(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<IDataReader> GetReader(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetReader(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<IDataReader> GetReader(string procedure, Dictionary<string, object> parameters)
        {
            return await GetReader(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<IDataReader> GetReader(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetReader(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataSet> GetDataSet(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataSet(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataSet> GetDataSet(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataSet(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataSet> GetDataSet(string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataSet(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataSet> GetDataSet(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataSet(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataTable> GetDataTable(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataTable(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataTable> GetDataTable(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataTable(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<DataTable> GetDataTable(string procedure, Dictionary<string, object> parameters)
        {
            return await GetDataTable(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<DataTable> GetDataTable(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetDataTable(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public void CloseConnection(DbConnection connection)
        {
            connection.Close();
            connection.Dispose();
            connection = null;
        }
        public void CloseConnection(DbTransaction Transaction)
        {
            Transaction.Dispose();
            Transaction.Connection.Close();
            Transaction.Connection.Dispose();
        }
        public async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await this.GetObjectList<T>(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await this.GetObjectList<T>(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<List<T>> GetObjectList<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await this.GetObjectList<T>(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<List<T>> GetObjectList<T>(string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await this.GetObjectList<T>(Query, CommandType.StoredProcedure, new Dictionary<string, object>());
        }
        public async Task<T> GetObject<T>(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await GetObject<T>(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<T> GetObject<T>(DbTransaction Transaction, string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetObject<T>(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<T> GetObject<T>(string procedure, Dictionary<string, object> parameters) where T : class, new()
        {
            return await GetObject<T>(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<T> GetObject<T>(string Query) where T : class, new()
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await GetObject<T>(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<object> ExecuteScalar(DbTransaction Transaction, string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteScalar(Transaction, procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<object> ExecuteScalar(DbTransaction Transaction, string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await ExecuteScalar(Transaction, Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<object> ExecuteScalar(string procedure, Dictionary<string, object> parameters)
        {
            return await ExecuteScalar(procedure, CommandType.StoredProcedure, parameters);
        }
        public async Task<object> ExecuteScalar(string Query)
        {
            if (Query.IndexOf("delete ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("Insert ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("alter ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("drop ", StringComparison.InvariantCultureIgnoreCase) > 0 ||
                 Query.IndexOf("update", StringComparison.InvariantCultureIgnoreCase) > 0)
                throw new Exception("Dangerous Query Detected...");
            return await ExecuteScalar(Query, CommandType.Text, new Dictionary<string, object>());
        }
        public async Task<Tuple<bool, string>> BulkInsert(DbTransaction Transaction, string Sql, DataTable dt)
        {
            bool IsSuccess = false;
            string error = null;
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = Transaction.Connection as SqlConnection;
                    cmd.Transaction = Transaction as SqlTransaction;
                    cmd.CommandText = Sql;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.UpdateBatchSize = dt.Rows.Count;
                        using (SqlCommandBuilder cb = new SqlCommandBuilder(da))
                        {
                            int i = da.Update(dt);
                        }
                        IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                IsSuccess = false;
                error = ex.Message;
            }
            return new Tuple<bool, string>(IsSuccess, error);
        }
        public async Task<Tuple<bool, string>> BulkAddOrUpdate<T>(string tblName, List<T> lstData, string Condition)
        {
            string Message = "Saved.";
            bool res = false;
            try
            {
                var Qry = "select* from " + tblName + " " + Condition;
                var table = await GetTableSchema(tblName);
                table = table.Fill(lstData);
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    var da = new SqlDataAdapter(Qry, con);
                    var builder = new SqlCommandBuilder(da);
                    var dt = new DataTable();
                    da.Fill(dt);
                    con.Open();
                    da.Update(table);
                    con.Close();
                    res = true;
                }
            }
            catch (Exception ex)
            {
                Message = "error occured: " + ex.Message + ex.StackTrace;
            }
            return Tuple.Create(res, Message);
        }
        public async Task<int> AddOrUpdate(string tblName, string Params)
        {
            return await ExecuteNonQuery(await GetAddOrUpdateCommond(tblName, Params));

        }
        public async Task<int> AddOrUpdate(string tblName, params object[] Params)
        {
            return await ExecuteNonQuery(await GetAddOrUpdateCommond(tblName, Params));
        }
        public async Task<List<string>> GetPK(string tblName)
        {
            List<string> res = new List<string>();
            var dt = (await GetDataSet("exec sp_pkeys " + tblName)).Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                res.Add(row["Column_name"].ToString());
            }
            return res;
        }
        public async Task<DataTable> GetTableInfo(string tblName)
        {
            return (await GetDataSet("sp_columns " + tblName)).Tables[0];
        }
        public async Task<bool> ChkRecords(string tblName, string Condition)
        {
            return Convert.ToInt32(await ExecuteScalar("select count(*) from " + tblName + Condition)) > 0;
        }
        public async Task<DataTable> GetTableSchema(string TableName)
        {
            DataTable Table = new DataTable();
            var dt = await GetDataSet("select * from " + TableName + "WHERE 1=0");
            if (dt.Tables.Count > 0)
            {
                return dt.Tables[0];
            }
            return Table;
        }
        public DbType GetDBType(System.Type theType)
        {
            System.Data.SqlClient.SqlParameter p1;
            System.ComponentModel.TypeConverter tc;
            p1 = new System.Data.SqlClient.SqlParameter();
            tc = System.ComponentModel.TypeDescriptor.GetConverter(p1.DbType);
            if (tc.CanConvertFrom(theType))
            {
                p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
            }
            else
            {
                //Try brute force
                try
                {
                    p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
                }
                catch (Exception)
                {
                    //Do Nothing; will return NVarChar as default
                }
            }
            return p1.DbType;
        }
        public DbCommand GetCommand(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = new SqlCommand(CommandText, Transaction.Connection as SqlConnection, Transaction as SqlTransaction);
            command.CommandType = commandType;
            command.CommandTimeout = 3600;

            command.Parameters.AddRange(GetDbParameter(parameters).ToArray());

            if (commandType == CommandType.Text)
            {
                command.Prepare();
            }

            return command;
        }
        public DbCommand GetCommand(DbConnection Connection, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = new SqlCommand(CommandText, Connection as SqlConnection);
            command.CommandType = commandType;
            command.CommandTimeout = 3600;
            command.Parameters.AddRange(GetDbParameter(parameters).ToArray());
            if (commandType == CommandType.Text)
            {
                command.Prepare();
            }
            return command;
        }
        public List<DbParameter> GetDbParameter(Dictionary<string, object> parameters)
        {
            List<DbParameter> Params = new List<DbParameter>();
            if (parameters != null)
            {
                foreach (var parameter in parameters.Keys)
                {
                    var name = parameter;
                    var p = new SqlParameter();

                    if (parameter.Contains("o:"))
                    {
                        p.Direction = ParameterDirection.Output;
                        name = parameter.Substring(parameter.IndexOf(':') + 1);
                    }

                    p.ParameterName = ParameterPrefix + name;
                    //p.ParameterName = name;
                    p.Value = parameters[parameter];

                    Params.Add(p);
                }
            }
            return Params;
        }

        private async Task<int> ExecuteNonQuery(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var result = 0;
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                result = await command.ExecuteNonQueryAsync();
                FillOutputParams(command, parameters);
            }
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            var result = await command.ExecuteNonQueryAsync();
            FillOutputParams(command, parameters);
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbCommand cmd)
        {
            var result = 0;
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {
                result = await cmd.ExecuteNonQueryAsync();
            }
            return result;
        }
        private async Task<int> ExecuteNonQuery(DbTransaction Transaction, DbCommand cmd)
        {
            var result = await cmd.ExecuteNonQueryAsync();
            return result;
        }
        private async Task<DbDataReader> GetReader(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            var reader = await command.ExecuteReaderAsync();
            FillOutputParams(command, parameters);
            return reader;
        }
        private async Task<IDataReader> GetReader(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            IDataReader reader = null;
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                reader = await command.ExecuteReaderAsync();
                FillOutputParams(command, parameters);
            }
            return reader;
        }
        private async Task<DataSet> GetDataSet(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            DbDataAdapter adr = new SqlDataAdapter(command as SqlCommand);
            adr.Fill(ds);
            FillOutputParams(command, parameters);
            return ds;
        }
        private async Task<DataSet> GetDataSet(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataSet ds = new DataSet();
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {

                var command = GetCommand(Connection, CommandText, commandType, parameters);
                SqlDataAdapter adr = new SqlDataAdapter(command as SqlCommand);
                adr.Fill(ds);
                FillOutputParams(command, parameters);
            }
            return ds;
        }
        private async Task<DataTable> GetDataTable(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTable dt = new DataTable();
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            SqlDataAdapter adr = new SqlDataAdapter(command as SqlCommand);
            adr.Fill(dt);
            FillOutputParams(command, parameters);
            return dt;
        }
        private async Task<DataTable> GetDataTable(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            DataTable dt = new DataTable();
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {

                var command = GetCommand(Connection, CommandText, commandType, parameters);
                SqlDataAdapter adr = new SqlDataAdapter(command as SqlCommand);
                adr.Fill(dt);
                FillOutputParams(command, parameters);
            }
            return dt;
        }
        private async Task<List<T>> GetObjectList<T>(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            List<T> list = new List<T>();
            var dt = await GetDataSet(Transaction, CommandText, commandType, parameters);
            if (dt.Tables.Count > 0)
            {
                list = dt.Tables[0].ToList<T>();
            }
            return list;
        }
        private async Task<List<T>> GetObjectList<T>(string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            List<T> list = new List<T>();
            var dt = await GetDataSet(CommandText, commandType, parameters);
            if (dt.Tables.Count > 0)
            {
                list = dt.Tables[0].ToList<T>();
            }
            return list;
        }
        private async Task<T> GetObject<T>(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            T Obj = new T();
            var lst = await GetObjectList<T>(Transaction, CommandText, CommandType.StoredProcedure, parameters);
            if (lst != null && lst.Count > 0)
            {
                Obj = lst[0];
            }
            return Obj;
        }
        private async Task<T> GetObject<T>(string CommandText, CommandType commandType, Dictionary<string, object> parameters) where T : class, new()
        {
            T Obj = new T();
            var lst = await GetObjectList<T>(CommandText, CommandType.StoredProcedure, parameters);
            if (lst != null && lst.Count > 0)
            {
                Obj = lst[0];
            }
            return Obj;
        }
        private void FillOutputParams(DbCommand command, Dictionary<string, object> parameters)
        {
            foreach (SqlParameter item in command.Parameters)
            {
                if (item.Direction == ParameterDirection.Output)
                {
                    parameters[$"o:{item.ParameterName}"] = item.Value;
                }
            }
        }
        private string GetCondition(List<string> Fields, List<object> Values, List<string> Pk)
        {
            string Condition = " Where ";
            for (int ii = 0; ii < Pk.Count; ii++)
            {
                object value = "";
                try
                {
                    value = Values[Fields.IndexOf(Pk[ii].ToUpper())];
                }
                catch (Exception)
                {

                }
                if (ii == 0)
                    Condition = Condition + Pk[ii] + " = '" + value + "'";
                else
                    Condition = " and " + Condition + Pk[ii] + " = '" + value + "'";
            }
            return Condition;
        }
        private async Task<object> ExecuteScalar(DbTransaction Transaction, string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            var command = GetCommand(Transaction, CommandText, commandType, parameters);
            object res = await command.ExecuteScalarAsync();
            FillOutputParams(command, parameters);
            return res;
        }
        private async Task<object> ExecuteScalar(string CommandText, CommandType commandType, Dictionary<string, object> parameters)
        {
            object res = null;
            using (DbConnection Connection = await GetConnection(ConnectionString))
            {
                var command = GetCommand(Connection, CommandText, commandType, parameters);
                res = await command.ExecuteScalarAsync();
                FillOutputParams(command, parameters);
            }
            return res;
        }
        private async Task<SqlCommand> GetAddOrUpdateCommond(string tblName, string Params)
        {
            JObject jsonData = JObject.Parse(HttpUtility.UrlDecode(Params));
            JToken jtoken = jsonData.First;
            DataTable tblInfo = await GetTableInfo(tblName);
            string FieldName;
            object FieldValue;
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            for (int i = 0; i < jsonData.Count; i++)
            {
                if (i == 0)
                    jtoken = jsonData.First;
                else
                    jtoken = jtoken.Next;
                FieldName = ((JProperty)jtoken).Name.ToString().ToUpper();
                FieldValue = ((JProperty)jtoken).Value.ToString();
                foreach (DataRow row in tblInfo.Rows)
                {
                    if (row["Column_Name"].ToString().Trim().ToUpper() == FieldName)
                    {
                        Fields.Add(FieldName);
                        ValuesParams.Add("@" + FieldName);
                        FieldsType.Add(row["Type_Name"].ToString().Trim());
                        if (row["Type_Name"].ToString().Trim() == "image")
                            FieldValue = FieldValue.ToString().GetByteArreyfromBase64Img();
                        Values.Add(FieldValue);
                        break;
                    }
                }
            }
            return await GetCmd(tblName, Fields, FieldsType, ValuesParams, Values);
        }
        private async Task<SqlCommand> GetAddOrUpdateCommond(string tblName, object[] Params)
        {
            DataTable tblInfo = await GetTableInfo(tblName);
            List<string> Fields = new List<string>();
            List<string> FieldsType = new List<string>();
            List<string> ValuesParams = new List<string>();
            List<object> Values = new List<object>();
            int i = 0;
            foreach (DataRow row in tblInfo.Rows)
            {
                Fields.Add(row["Column_Name"].ToString().Trim());
                FieldsType.Add(row["Type_Name"].ToString().Trim());
                ValuesParams.Add("@" + row["Column_Name"].ToString().Trim());
                if (Params.Length <= i)
                    continue;
                //if (row["Column_Name"].ToString().Trim().Contains("identity"))
                //    Values.Add(GetIdentity(tblName));
                //else
                Values.Add(Params[i++]);
            }
            return await GetCmd(tblName, Fields, FieldsType, ValuesParams, Values);
        }
        private async Task<SqlCommand> GetCmd(string tblName, List<string> Fields, List<string> FieldsType, List<string> ValuesParams, List<object> Values)
        {
            var Pk = await GetPK(tblName);
            string Condition = GetCondition(Fields, Values, Pk);
            foreach (var item in Pk)
            {
                if (Fields.Contains(item.ToUpper()))
                {
                    var index = Fields.IndexOf(item.ToUpper());
                    Fields.RemoveAt(index);
                    Values.RemoveAt(index);
                }
            }
            string Qry = "";
            if (await ChkRecords(tblName, Condition))
            {
                Qry = "Update " + tblName + " set ";
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (Pk.IndexOf(Fields[ii]) >= 0)
                        continue;
                    if (Qry == "Update " + tblName + " set ")
                    {
                        Qry = Qry + Fields[ii] + " = " + ValuesParams[ii];
                    }
                    else
                    {
                        Qry = Qry + "," + Fields[ii] + " = " + ValuesParams[ii];
                    }
                }

                Qry = Qry + Condition;
            }
            else
            {
                for (int ii = 0; ii < Fields.Count; ii++)
                {
                    if (FieldsType[ii].Trim().Contains("identity"))
                    {
                        Fields.RemoveAt(ii);
                        ValuesParams.RemoveAt(ii);
                        Values.RemoveAt(ii);
                        FieldsType.RemoveAt(ii);
                    }
                }
                Qry = "insert into " + tblName + " (" + string.Join(",", Fields) + ") values (" + string.Join(",", ValuesParams) + ")";
            }
            SqlCommand cmd = new SqlCommand(Qry, new SqlConnection(ConnectionString));
            for (int ii = 0; ii < ValuesParams.Count; ii++)
            {
                if (Values.Count > ii)
                    cmd.Parameters.AddWithValue(ValuesParams[ii], Values[ii]);
                else
                {
                    if (FieldsType[ii] == "image")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], new byte[10]);
                    else if (FieldsType[ii] == "datetime")
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DateTime.Now);
                    else
                        cmd.Parameters.AddWithValue(ValuesParams[ii], DBNull.Value);
                }
            }
            return cmd;
        }
        private DataTable UpdateDataTable(DataTable table, DataTable Source, List<string> PK)
        {
            DataRow dr;
            if (PK != null)
            {
                return table;
            }
            foreach (DataRow item in Source.Rows)
            {
                List<DataRow> a = null;
                #region PK Condition
                if (PK.Count == 1)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]))
                    .ToList();
                }
                else if (PK.Count == 2)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]))
                    .ToList();
                }
                else if (PK.Count == 3)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]))
                    .ToList();
                }
                else if (PK.Count == 4)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]))
                    .ToList();
                }
                else if (PK.Count == 5)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]) &&
                    x.Field<int>(PK[4]) == item.Field<int>(PK[4]))
                    .ToList();
                }
                else if (PK.Count == 6)
                {
                    a = table.AsEnumerable().Where(x =>
                    x.Field<int>(PK[0]) == item.Field<int>(PK[0]) &&
                    x.Field<int>(PK[1]) == item.Field<int>(PK[1]) &&
                    x.Field<int>(PK[2]) == item.Field<int>(PK[2]) &&
                    x.Field<int>(PK[3]) == item.Field<int>(PK[3]) &&
                    x.Field<int>(PK[4]) == item.Field<int>(PK[4]) &&
                    x.Field<int>(PK[5]) == item.Field<int>(PK[5]))
                    .ToList();
                }
                //a = table.AsEnumerable().Where(x =>
                //x.Field<int>("BankID") == item.Field<int>("BankID") &&
                //x.Field<int>("CityID") == item.Field<int>("CityID") &&
                //x.Field<int>("FID") == item.Field<int>("FID"))
                //.ToList();
                #endregion

                if (a != null && a.Count > 0)
                {
                    foreach (DataColumn col in item.Table.Columns)
                    {
                        a[0][col.ColumnName] = item[col.ColumnName];
                    }
                }
                else
                {
                    dr = table.NewRow();
                    foreach (DataColumn col in item.Table.Columns)
                    {
                        dr[col.ColumnName] = item[col.ColumnName];
                    }
                    table.Rows.Add(dr);
                }
            }
            return table;
        }
        public void ErrorLog(string Error)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    string Qry = "";
                    SqlCommand cmd = new SqlCommand("select * from ErrorLog", con);
                    DataTable dt = new DataTable();
                    (new SqlDataAdapter(cmd)).Fill(dt);
                    if (dt.Rows.Count > 1000)
                    {
                        Qry = "delete from ErrorLog; ";
                    }
                    Qry = Qry + "insert into ErrorLog (Error,ErrorTime) values('" + Error.Replace("'", "''") + "', getdate() )";
                    con.Open();
                    cmd.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

            }
        }

    }
}
